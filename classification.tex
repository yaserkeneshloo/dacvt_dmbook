\chapter{Classification}
In the classification task, we are dealing with two types of datasets. The training dataset is used to train a model over a dataset and the test dataset is used to examine the performance of the trained model. In the training phase, we are given a training dataset $D=\{d_1,d_2,\cdots,d_n\}$ of instances, samples, rows or examples. Each instance in $D$ is described by values from a feature/attribute set $F={f_1,f_2,\cdots,f_k}$. Moreover, each instance is labeled with a class from a mutually exclusive set $C={c_1,c_2,\cdots,c_l}$. Therefore, the classification task is defined as learning a distinctive pattern from the training dataset and using that pattern for classifying all the new instances in the test dataset.\\
\section{Applications of classification}
There are various applications for classification. As an example, the problem of identifying whether a person will have a good or bad credit score can be considered as a two-class classification problem. We can consider some personal information such as marital status, age, annual income, debt amount, city, state, profession, and etc as the features for this classification problem. There are applications in spam email detection (spam vs not spam), movie review classification (good, bad, or nasty), gene classification (carcinogen vs benign), Twitter account (fake vs real).
The mapping in a classification problem is a mapping from input features to a discrete value representing the class label for the input. Despite other methods like regression, it does not provide any continuous value for an input. For instance, in the example of credit score we are not trying to find an estimate for the credit score of a person, rather we are trying to classify a person as someone who can have a high or low credit score.
\section{Input and Outputs}
Before any classification process, one should know the data and characterize the variables in the dataset that she is trying to deal with. Each dataset can contain different type of features with different data types. Each dataset can be made from any mix of continuous and discrete variables. Usually, these data types are categorized in the following four categories:
\begin{enumerate}
\item{Nominal}: This data type is for attribute values that cannot be compared. For instance, color is nominal value, since you cannot compare red to green. We cannot say whether green is bigger than red or vise versa. As an another example, name of a person considered as a nominal attribute, since you cannot compare two instances only based on their names.
\item{Ordinal}: The ordinal data type is like a nominal value, with the difference that order of each value is also a matter of importance. As an example, we can consider the credit score of a person as being very low, low, medium, high, and very high. Although there is a notion of order in this data type, we cannot say how much bigger is low from very low.
\item{Interval}: In the interval data type, we can say the difference between two values but still we do not have an absolute sense of zero. As an example, temperature considered as an interval data type. We can distinguish between 20 degrees Fahrenheit with 30 degrees Fahrenheit, but we do not have a sense of absolute zero for temperature. Zero degrees Fahrenheit does not mean that it is the lowest point for this feature. It is considered as just another point in the scale.
\item{Ratio}: This data type is like the interval data type except it has a sense of absolute zero. As an example of this data type, weight of a person is considered as a ratio data type.
\end{enumerate}
\section{Wrong assumption on classification}
There are some assumptions that people make in a classification problem, but some of them are typically wrong. Some of these assumption are listed as follows:
\begin{enumerate}
\item{Train/Test distribution}: The first assumption is that the data from the training and test sets are drawn from the same distribution. This assumption does not necessary hold for all the datasets. One could think about a teacher that teaches some materials in a class, but use the materials that he did not teach in the class on the exam.\\
In most of the data mining problems, the training and test data are not drawn from two different distribution.
\item{One class labeling}: In some of the data mining problems, one instance can have multiple class labels. For instance, in image tagging, a picture of a house in a jungle can be descriptive of both the nature and the house.
\item{Misclassification error}: In most of the data mining problem, the cost of misclassification is different between each class. For instance, in a medical application, think about the cost of identifying a healthy person as sick versus the cost of identifying a sick person as healthy? Are these two costs the same?
\end{enumerate}
\section{Bayesian Approach}
In the Bayesian approach, we use the Bayesian formula to find the posterior probability of a class given all the data in the dataset. Generally speaking, given an instance $d$ and a class $c$, we want to solve the following maximization problem:
\begin{eqnarray*}
c_{MAP} & = & \arg\max_{c \in C} P(c|d) \nonumber \\
& = & \arg\max_{c \in C} {{P(d|c) P(c)}\over{P(d)}} \nonumber \\
& = & \arg\max_{c \in C} {{P(d|c) P(c)}} \nonumber
\end{eqnarray*}
In the above equation, second line achieved by applying the Bayes rule and since $P(d)=\sum_{c}P(d|c)P(c)$ is a constant value amongst all the classes, it can be removed from the maximization equation. This means that we are looking for a class $c$ that will maximize the $P(c|d)$. As mentioned before, $d$ is comprised of $|F|$ features. By expanding $d$ to these $|F|$ features, the maximization looks like the following:
\begin{eqnarray*}
c_{MAP} & = & \arg\max_{c \in C} {{P(d|c) P(c)}} \nonumber \\
& = & \arg\max_{c \in C} P(f_1,f_2,\ldots,f_{|F|}|c) P(c) \nonumber
\end{eqnarray*}
 If we take a look at the above equation, to determine the class of an instance, one of the things that we need to know is the prior probability of a class, i.e. $P(c)$. This can be reckoned easily give the dataset, by counting the number of instance in a class divided by total number of instances. However, calculating $P(f_1,f_2,\cdots,f_{|F|}|c)$ is not feasible. To understand the reason behind it, consider a 3 classes dataset with 10 features. Each feature has 6 different possible value. According to the above equation, we need to estimate  $3\times6^{10}+3\approx$ 181 million parameters to find the posterior probabilities for the whole dataset. Although finding this number of parameters is a time-consuming process, it is not an unrealistic estimate. For instance, to classify the credit score of a person, we are asking about age, salary, monthly rent, and etc, then we will calculate above-mentioned probability to find the credit score class of this person. On the other hand, to have a good estimate of the above probability, we should have a really big dataset to cover almost all the possible combinations of each feature.\\
\subsection{Naive Bayes}
To alleviate this problem, we will make some assumptions about the dataset and its feature. The assumption that we are making for the above problem will create a new classifier which is called Naive Bayes classifier. According to the Naive Bayes assumption, features are {\bf conditionally independent give the class}:
\begin{eqnarray*}
P(f_1,f_2,\cdots,f_{|F|}|c) & = & P(f_1|c) P(f_2|c) P(f_3|c) \ldots P(f_{|F|}|c) \nonumber \\
c_{MAP} & = & \arg\max_{c \in C} P(f_1,f_2,\ldots,f_{|F|}|c) P(c) \nonumber \\
c_{NB} & = & \arg\max_{c \in C} P(c) \prod_{i=1}^{|F|} P(f_i|c) \nonumber
\end{eqnarray*}
According to the above assumption, after knowing the class label for an instance, all the features are independent of each other. For instance, if someone has a good credit score, we can tell everything about that person based on the distribution of each feature. Therefore, in this assumption, we do not need to know the inter-relationship between each feature.\\
Based on the above equation, we now estimate the number of parameters that is required in the Naive Bayes assumption. According to the $c_{NB}$ equation, we require $3\times 6\times 10 + 3$ parameters estimation to determine the class of all instances in the dataset. This number is much smaller than 181 million parameters that are required by the MAP approach.\\
However, when we want to calculate the $c_{NB}$, we should avoid to multiplying probabilities. Since each probability is a number between 0 and 1, multiplying a lot of probabilities will lead to very small numbers which are mostly close to zero. Because of this floating point underflow, we will use the monotonicity-preserving property of the logarithm function to calculate the $c_{NB}$. The values that make a function maximized will make the log of its function maximized:
\begin{eqnarray*}
c_{NB} & = & \arg\max_{c \in C} P(c) \prod_{i=1}^{|F|} P(f_i|c) \\
c_{NB} & = & \arg\max_{c \in C} ( \log P(c) + \sum\limits_{i=1}^{|F|} \log P(f_i|c) )
\end{eqnarray*}
As we mentioned in this section, Naive Bayes consider the conditional independence assumption for features. But, is this assumption a strong assumption? Although this assumption is not strong, it does not affect the ordinal nature of classification boundary. Even though the probability estimations are wrong, the decisions are right. To prove why the Naive Bayes works good even with these atrocious assumptions, we follow the proofs that is given by Domingos and Pazzani \cite{}. In this paper, the author shows that the features do not need to be necessary independent for Naive Bayes to be optimal. Consider a dataset with three boolean features $f_1$, $f_2$, and $f_3$ and two equiprobable classes $c_1$ and $c_2$. Assume that $f_1=f_2$ and $f_1$ and $f_3$ are conditionally independent given $c$. Let's see how the full Bayes classifier do in this dataset. The optimal Bayes classifier exploits the fact that $f_1=f_2$ and utilize only two features $f_1$ and $f_3$ to classify instances. We classify an instance into class $c_1$ if the following is true:
\begin{eqnarray*}
P(f_1,f_3|c_1) P(c_1) & > & P(f_1,f_3|c_2) P(c_2)
\end{eqnarray*}
The prior probability of two classes is the same, then
\begin{eqnarray*}
P(f_1,f_3|c_1) & > & P(f_1,f_3|c_2) \\
P(f_1|c_1) P(f_3|c_1) & > & P(f_1|c_2) P(f_3|c_2)\\
{{P(c_1|f_1) P(f_1)} \over{P(c_1)}} {{P(c_1|f_3) P(f_3)} \over{P(c_1)}} & > & {{P(c_2|f_1) P(f_1)} \over{P(c_2)}} {{P(c_2|f_3) P(f_3)} \over{P(c_2)}}\\
P(c_1|f_1) P(c_1|f_3) & > & P(c_2|f_1) P(c_2|f_3)
\end{eqnarray*}
By replacing $p=P(c_1|f_1)$ and $q=P(c_1|f_3)$:
\begin{eqnarray*}
pq & > & (1-p) (1-q)\\
q & > & (1-p)
\end{eqnarray*}
However, the Naive Bayes assumes aims to use all three features. It makes some correct assumptions like $f_1$ and $f_3$ are conditionally independent, but it also makes the wrong assumption that $f_1$ and $f_2$ are conditionally independent. If we do the same calculation for the Naive Bayes classifier, we have the following equations:
\begin{eqnarray*}
P(f_1,f_2,f_3|c_1) P(c_1) & > & P(f_1,f_2,f_3|c_2) P(c_2) \\
P(f_1,f_2,f_3|c_1) & > & P(f_1,f_2,f_3|c_2) \\
P(f_1|c_1) P(f_2|c_1) P(f_3|c_1) & > & P(f_1|c_2) P(f_2|c_2) P(f_3|c_2)\\
P(f_1|c_1)^2 P(f_3|c_1) & > & P(f_1|c_2)^2 P(f_3|c_2)\\
{[{P(c_1|f_1) P(f_1)]^2} \over{P(c_1)^2}} {{P(c_1|f_3) P(f_3)} \over{P(c_1)}} & > & {[{P(c_2|f_1) P(f_1)]^2} \over{P(c_2)^2}} {{P(c_2|f_3) P(f_3)} \over{P(c_2)}}\\
P(c_1|f_1)^2 P(c_1|f_3) & > & P(c_2|f_1)^2 P(c_2|f_3)\\
p^2q & > & (1-p)^2 (1-q)\\
q & > & {(1-p)^2}\over{p^2 + (1-p)^2}
\end{eqnarray*}
Now if we plot the $q$ versus $p$ for these two classifiers, we can see that these two classifiers intersect in three points. Moreover, we can see that only in a small portion of this area, these two classifiers make different decisions. This is the area between the line of optimal Bayes and Naive Bayes. According to the above example, we can see that although the probabilities are wrong, the decision of classification is right.
\subsection{Text Classification using Naive Bayes}
One of classical application of NB in practice is on text classification. Therefore, as an example when someone wants to classify emails, tweets, posts, comments, or reviews, she can apply NB on the dataset. Before all of the text classification, one should do some cleaning of the text. The cleaning process is usually considered the following steps:
\begin{enumerate}
\item{Formatting}: In this phase, we remove all the junk formatting and unknown characters from the text
\item{standardizing case}: Should we make all the words in the text capitalized or lower cased or leave them as is?
\item{tokenization}: This phase is about to split the text into words. It will take each sentence and usually separate the text using white spaces. During this phase, we should define certain rules for dealing with special statements like "Dear Mr. O'Neill". Should we consider the comma in this sentence as part of the name or as a character that needs to be removed?
\item{Stopword removal}: In this phase, we will remove all the common words that can be found in any types of texts. Words like "the", "I", "me", "and", and etc. These are the kind of words that can be found in every text and will not help in classifying two different documents.
\item{Normalization}: In this phase, we will take care of accents and diacritics correction. The last but not least is the stemming and lemmatization. In stemming phase, we are trying to find the root of different words and use the root instead of all the other different formation of a word. For instance, after stemming the word "classify" will replace instead of words like "classification", "classifier", and "classified". Lemmatization is a similar process as stemming, however it will determine the lemma for a given word and it is usually consist of complex understanding of text and determining the part of speech of a word in a sentence. Using this technique, words like "worse" will be replaced by "bad". These are the kind of relation that are missing in the stemming process and it is handled by lemmatization.
\end{enumerate}
After cleaning all the documents based on the above techniques, we should find a way to represent a document as a set of features. There are different features that we can consider when representing a document. For instance, one should ask whether the position of a word in the document matters or not? The answer could be different based on the application that we are looking for. In classifying the spam emails, it does not matter where the word "mortgage" appeared in the text. Moreover, rather asking whether a word appeared in the document or not, we can ask the number of times that it appeared in the text.
\subsection{Bag of Words}
As mentioned above, when representing a document not only the appearance of a word in a document matters, but also the number of times that it appeared in the document considered as an important fact. Moreover, we mentioned that the position of a word in a text is not an important factor. These two assumptions, i.e. positional independence and multinomial occurrences, are considered as extra assumptions beyond the conditional independence that is presumed by the Naive Bayes. We call the positional independence assumption as \emph{Bag of words}, since the only things that is important in this assumption are the words and the number of times that they appeared in a text.\\
By preparing our data using the bag of words model, we can classify each document as follows:
\begin{eqnarray*}
\hat{P}(c) & = & {1\over{|D|}} \sum\limits_{i=1}^{|D|} \delta(\textrm{class}(d_i) = c) \\
\end{eqnarray*}
where $\delta(.)$ function returns one when the class of document $d_i$ is equal to $c$ and zero otherwise. Since this is just an estimation of the real priors in a classification task we are using the $\hat{P}(c)$ rather than $P(c)$. To find the conditional probability of each feature we can use the following equation:
\begin{eqnarray*}
\hat{P}(f_i|c) & = & \textrm{occurrences}(f_i,c) \over {\sum\limits_{f_i'} \textrm{occurrences}(f_i',c)}\\
\end{eqnarray*}
where the $occurrences(f_i,c)$ is the number of times word $f_i$ occurred in documents of class $c$.\\
The problem with the above formulation is that for some words that do not exist in the training data, the nominator of the above equation would be zero for both classes. Therefore, we cannot classify this instance because of this unseen word. To alleviate this problem, some smoothing techniques is used to change the number of occurrences of a word. In the following section, we explain two of these smoothing techniques:
\subsection{Laplace Smoothing}
The first smoothing techniques known as Laplace smoothing or add-one smoothing. In this approach, we consider at least one occurrence for each word:
\begin{eqnarray*}
\hat{P}(f_i|c) & = & {\textrm{occurrences}(f_i,c) + 1} \over {\sum_{f_i'} ({\textrm{occurrences}(f_i',c) + 1})}\\
& = & {\textrm{occurrences}(f_i,c) + 1} \over {(\sum_{f_i'} {\textrm{occurrences}(f_i',c)) + |F|}}
\end{eqnarray*}
In the above formulation, for each word we add the occurrence of it by one. This way we will make sure that all words in the dataset have at least one occurrence in our dataset. On the other hand, to correct the normalization factor, we add $|F|$ to the denominator of the previous equation. In this approach, it saves some small probabilities for words that did not happen in the training data. This can be considered as starting with a uniform distribution as the prior distribution for $P(f_i|c)$ and then updating the distribution as more training data is processed.
\subsection{Good-Turing estimator}
Although we fixed the problem of zero occurrences for a word using the Laplace smoothing, there are some problems attached to this approach. Laplace smoothing gives too much weight to unseen words. To fix this problem, I.J. Good proposed another smoothing method to find the probability of $P(f_i|c)$. Based on this idea, if two words appear equal number of times in the dataset, they should have the same probability. In this way, we group each words into equivalence classes. To see how this idea works in action, consider the following example:\\
Consider a dataset with dictionary of words as \{apple, banana, carrot, date, egg, fish, grape, ham\}. Then consider three documents in class $c$ with the following words in each document: $d_1$=\{apple,banana,carrot\}, $d_2$=\{date, carrot, fish\}, $d_3$=\{grape,apple,egg\}. In the GT-estimation, we find the number of words that appear exactly $r$ times in all the documents of class $c$ and represent this number as $N_r$. Therefore, the total number of words in documents of class $c$ can be calculated using the $N = \sum_{r} rN_r$. For the above example, word "ham" did not appear in a document, therefore $N_0 = 1$. Words "banana", "date", "fish", "grape" and "egg" appeared once in this dataset, therefore $N_1=5$. Finally, words "apple" and "carrot" appeared twice in this dataset and as a result $N_2 = 2$.\\
According to the GT-estimation, after finding all the words and their respective $N_r$, the probability of a word appearing given that it appears $r$ times in documents of class $c$ is estimated as follows:
\begin{eqnarray*}
\hat{\theta}_{r} & = & {1\over{N}} (r+1) {{E[N_{r+1}]}\over{E[N_r]}}  \\
\end{eqnarray*}
In the above equation, the probability of a word that occurred $r$ times in a dataset is determined by the higher value of $r$, i.e. $E[N_{r+1}]$. We can see how the GT-estimator works with comparing its probability calculation w.r.t Laplace smoothing and the MLE assumption in Table \ref{table:mlelapgt}. As you can see in this table, the GT estimator gives zero probability to words that occurred the most in a dataset. It might seems as a disadvantage of this approach but when you rank words of a big corpus based on this estimator, you will see that the probabilities of each word is close to the one calculated in MLE.
\begin{table}[!bh]
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
$r$ & Words & MLE & Laplace & Good-Turing\\
\hline\rule{0pt}{12pt}
0 & Ham & 0 & 1/17 & 5/9\\
1 & Banana, Date, & & &\\
   & Egg, Fish, & 1/9 & 2/17 & 4/9\\
   & Grape & & &\\
2 & Apple, Carrot & 0 & 3/17 & 0(!)\\
\hline
\multicolumn{2}{|c|}{Marginalize} & 5(1/9)+2(2/9) = 1 & 1/17 + 5 (2/17) + 2(3/17)= 1 & 5/9+4/9+0 =1\\
[2pt]
\hline
\end{tabular}
\caption{Comparison of the $P(f_i|c)$ using different smoothing approaches}
\label{table:mlelapgt}
\end{center}
\end{table}

{\bf The GT-estimator assumes that the underlying distribution is binomial. NEED MORE EXPLANATION ON THIS}\\
In practice, we usually use the the GT-estimator when the frequency of a word is less than a given threshold. For words that the actual frequencies are greater than a given threshold, we usually use the values from Laplace smoothing. Another way to use this estimator is to fit a function for estimating $N_r$ as $N_r = ar^b$ with some value of $b<-1$. In this case, we fit the curve for high values of $N_r$ and use the measured $N_r$ directly for small values. Note that after all of the above-mentioned processes, we should normalize the probabilities so that they sum up to one and we have a valid distribution over the data.
\subsection{Bernoulli Model}
In the Bernoulli representation of data, only the presence or absence of a term in a document matters. Therefore, we are not counting the number of times that a word occurred in a document. This model, affects the estimation as well as classification of documents since we are loosing a lot of data by discarding the number of times a word is occurred in the data. On the other hand, a document might be assigned to a class based on a single word. Moreover, this approach explicitly models the absence of a word as opposed to Multinomial Naive Bayes. Thus it makes more mistakes in classifying large documents.

\subsection{Feature Selection}
In a text classification application, there are hundreds of thousands of features or words in a dataset. But, not all of these words are considered as a good discriminator for the classification task. Therefore, before any classification task, we should find the most important and useful features and use those for classifying the documents.\\
There are different strategies that can be used for feature selection in a given dataset. Taking the mutual information between terms and classes, the Chi-squared test between terms and classes which finds the Pearson's correlation among each feature, and even frequency of terms in a given class could be considered as measures that we can use for the feature selection. Usually, we rank the best features for each class and then take the union of them into a set and consider it as the final selected features for the classification task. However, when using these approaches, we should be careful about the assumptions that each of these approaches have made to find the best features. Most of these approaches are based on null-hypothesis assumptions and not only the measured value does matter for them, the p-value or level of confidence about the result also matters.

\subsection{NB for continuous-valued features}
In the previous sections, all the features that we used were discrete values. In order to use Naive Bayes for continuous valued features, we should make the assumption that each feature is drawn from a Gaussian distribution with a specific mean and variance given a specific class:
\begin{eqnarray*}
\mu_{ij} & = & E[f_i|C=c_j], \,\,\,\,\,\,i \in \{1,\cdots,|F|\}, j \in \{1,\cdots,|C|\}\\
\sigma_{ij}^2 & = & E[(f_i - \mu_{ij})^2|C=c_j]
\end{eqnarray*}
In the above equation, we are trying to find the mean and variance of each feature given a specific class. To find each of these values we use the following equations:
\begin{eqnarray*}
\hat{\mu}_{ij} & = & {1 \over {\sum\limits_{l=1}^{|D|} \delta(\textrm{class}(d_l)=c_j)}} \sum\limits_{l=1}^{|D|} f_i(d_l) \delta(\textrm{class}(d_l)=c_j)\\
\hat{\sigma}_{ij}^2 & = & {1 \over {\sum\limits_{l=1}^{|D|} \delta(\textrm{class}(d_l)=c_j)}} \sum\limits_{l=1}^{|D|} (f_i(d_l)-\hat{\mu}_{ij})^2 \delta(\textrm{class}(d_l)=c_j)\\
\end{eqnarray*}
According to the above equation, the sample variance that is a biased estimation of variance {\bf NEED MORE EXPLANATION WHY?}.To tackle this problem, we usually use the unbiased estimation of the sample variance as follows:
\begin{eqnarray*}
\hat{\sigma}_{ij}^2 & = & {1 \over ({\sum\limits_{l=1}^{|D|} \delta(\textrm{class}(d_l)=c_j)}) - 1} \sum\limits_{l=1}^{|D|} (f_i(d_l)-\hat{\mu}_{ij})^2 \delta(\textrm{class}(d_l)=c_j)
\end{eqnarray*}

In general, we have a mix of discrete and continuous values in a dataset. To discretize a continuous feature we should bucket them into equal-depth buckets. Then we can apply the Naive Bayes as mentioned in the previous section on all the discrete valued features in the dataset.\\
Another issue that we should usually tackle in a dataset is how to deal with the missing values in a dataset. There are lots of algorithms that find an estimate for a missing value in a dataset. But, this process should be done before any classification task.
\subsection{Evaluation Strategy}
So far we learned how to train a model using the Naive Bayes classifier given a dataset. But we have not talked about how to evaluate the performance of a classifier. Usually, we separate the instances in a dataset to three disjoint sets, known as train set, validation set, and test set. We train the model using the instances in the train set and tune the performance of the classifier using the validation set and after that we will apply the model on truly unseen instances that exist in the test set to see the result of the classification.\\
To measure the performance of a classifier there are different measures that can be used. In order to define each of these measures, let's first divide the classified instances of a two classes dataset into four different categories as follows:
\begin{enumerate}
\item{True Positive (TP)}: These are the number of instances that belong to the positive class and the classifier classified them as positive.
\item{False Positive (FP)}: These are the number of instances that belong to the positive class and the classifier classified them as negative.
\item{False Negative (FN)}: These are the number of instances that belong to the negative class and the classifier classified them as positive.
\item{True Negative (TN)}: These are the number of instances that belong to the negative class and the classifier classified them as negative.
\end{enumerate}
Given these measures we can create a matrix for each dataset, known as the \emph{confusion matrix}:
\begin{table}[!bh]
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
 & Labeled P & Labeled N\\
\hline
Classified P & TP & FP\\
\hline
Classified N & FN & TN\\
[2pt]
\hline
\end{tabular}
\caption{The confusion matrix over a dataset}
\label{table:confusion}
\end{center}
\end{table}
Using the confusion matrix, we can define different measure of performance for a classifier:
\begin{enumerate}
\item{Recall}: The recall is defined as follows and determines the portion of positive instances that are classified correctly.
\begin{eqnarray*}
recall = \frac{TP}{TP+FN}
\end{eqnarray*}
\item{Precision}: The precision is defined as follows and determines how many of the instances that are classified as positive really belong to the positive class.
\begin{eqnarray*}
precision = \frac{TP}{TP+FP}
\end{eqnarray*}
\item{F-statistic}: This measure is the harmonic mean of recall and precision. The reason for using the F-statistic is that neither recall nor precision alone can be a good merit for a dataset. A classifier can easily have a high value of recall by classifying every instance into positive class. Therefore, F-statistic tries to find a harmonic mean from these two measures and the only way that this measure can have a high value is that both the precision and recall should have a high value in a dataset. This measure is defined as follows:
\begin{eqnarray*}
F-statistic = 2.\frac{Precision.Recall}{Precision+Recall}
\end{eqnarray*}
In a multi class problem, the confusion matrix would have the size of $|c|\times|c|$.
\end{enumerate}
Usually, having only one train and validation set gives a biased result due to the random nature of dividing the dataset. To avoid this problem, we divide the dataset into $k$ equal size parts which is called "folds". Then, we use one fold as the test set and all the other $k-1$ parts for training set. We compute the performance measure for each of these folds and then aggregate the statistics across all folds to get the final performance result. This method is called cross-validation and if we use $k$ folds to partition the data, it is called $k$-fold cross validation. In a special case, when we use only one instance for testing the model and the rest of instances as the training set, it is called "leave-one-out".
\subsection{Theoretical understanding of Naive Bayes}
In this section, we provide more insight on the details of the NB algorithm. The Naive Bayes Classifier (NBC) is considered as a generative classifier. The reason that NBC is considered as a generative classifier is that it models a specific distribution. It models the probability of features as a class. The reason that is called a generative classifier is that using the model that is created by this classifier we can create a hypothetical document. For instance to make a document using the probabilities, we need to pick up a class, positive or negative. After choosing the class, we can use the probability of each feature to make the hypothetical document. On the opposite side, there is another type of classifiers which is called discriminative classifiers. These classifiers do not care about how the classifier is generated, but how to classify each instance into the classes. In a discriminative classifier we care about the probability of a class give the features rather than features give the class. These classifier directly calculate the posterior probability of a class given features. One of the discriminative classifier is the logistic regression classifier which will be explained later on in this chapter.\\
As we mentioned earlier, the NBC is motivated from perspective of the Bayesian decision theory. Consider a two-class scenario. The Bayes rule is as follows:
\begin{eqnarray*}
P(c|d) & = & {P(d|c) \times P(c)} \over {P(d)} \\
\textrm{posterior} & = & {\textrm{likelihood} \times \textrm{prior}} \over \textrm{evidence} \\
\end{eqnarray*}
We call the $P(c|d)$ as the posterior probability, the $P(c|d)$ as the likelihood, $P(c)$ as the prior probability and $P(d)$ as the evidence. Let's define the probability of having an error in this definition. We will face an error if we predict an instance of class $c_1$ as $c_2$ or predict an instance of class $c_2$ as $c_1$:
\begin{eqnarray*}
P(\textrm{error}|d) = \left\{
\begin{array}{lr}
    P(c_2|d) & \textrm{if we predict } c_1\\
    P(c_1|d) & \textrm{if we predict } c_2
  \end{array}
\right.
\end{eqnarray*}
Given this definition, we are looking to minimize the overall $P(error)$. We can rewrite the $P(error)$ as follows:
\begin{eqnarray*}
P(\textrm{error}) & = & \sum\limits_{i=1}^{|D|} P(\textrm{error},d) \\
& = & \sum\limits_{i=1}^{|D|} P(\textrm{error}|d) P(d) \\
\end{eqnarray*}
In the above equation, for each instance we find the marginal probability of $P(error,d)$. To get a minimized value for the $P(error)$ in the ideal case, for every instance $d$, $P(error|d)$ should be as small as possible. Therefore, given the assumption that two classes have the same prior distribution, i.e. $P(c_1)=P(c_2)$ we predict an instance into a class based on the following criteria:
\begin{eqnarray*}
\textrm{Predict } c_1 &\textrm{if} & P(c_1|d) > P(c_2|d)\\
\textrm{Predict } c_2 &\textrm{if} & P(c_2|d) \ge P(c_1|d)\\
\end{eqnarray*}
With this strategy, error is defined as:
\begin{eqnarray*}
P(error|d) = \min(P(c_1|d),P(c_2|d))
\end{eqnarray*}
In the above equation, the class that makes the minimize the above equation determines the misclassification error. In the case that $P(c_1)\neq P(c_2)$ the strategy changes as the following:
\begin{eqnarray*}
\textrm{Predict } c_1 &\textrm{if} & P(d|c_1) P(c_1) > P(d|c_2) P(c_2) \\
\textrm{Predict } c_2 &\textrm{if} & P(d|c_2) P(c_2) \ge P(d|c_1) P(c_1) \\
\end{eqnarray*}
We can use this idea to quantify the risk of classification. As mentioned earlier, the misclassification carries different costs based on the application. We represent the risk in classifying instance $d$ as class $c_1$ and $c_2$ as $R(c_1,d)$ and $R(c_2,d)$, respectively. We can denote these risks using the following equations:
\begin{eqnarray*}
R(c_1,d) & = & \lambda_{11} P(c_1|d) + \lambda_{12} P (c_2|d) \\
R(c_2,d) & = & \lambda_{21} P(c_1|d) + \lambda_{22} P (c_2|d) \\
\end{eqnarray*}
In the above equation, $\lambda_{12}$ denotes the loss that is incurred in deciding that the class is $c_1$ when the class is really $c_2$. Usually $\lambda_{11}$ and $\lambda_{22}$ has no risks and we can assign an equal value to them. However, we should make sure that either $\lambda_{12}-\lambda_{21}>0$ or $\lambda_{12}-\lambda_{21}<0$ is satisfied in our problem. After finding each of these loss values, we decide an instance belongs to class $c_1$ if:
\begin{eqnarray*}
R(c_1,d) & < & R(c_2,d)\\
\lambda_{11} P(c_1|d) + \lambda_{12} P (c_2|d) & < & \lambda_{21} P(c_1|d) + \lambda_{22} P (c_2|d) \\
(\lambda_{21} - \lambda_{11}) P(c_1|d) & > & (\lambda_{12} - \lambda_{22}) P(c_2|d) \\
(\lambda_{21} - \lambda_{11}) P(d|c_1) P(c_1) & > & (\lambda_{12} - \lambda_{22}) P(d|c_2) P(c_2) \\
{P(d|c_1)\over{P(d|c_2)}} & > & {{(\lambda_{12} - \lambda_{22})}\over{(\lambda_{21} - \lambda_{11})}} {P(c_2)\over{P(c_1)}}\\
\end{eqnarray*}
The final equation is a likelihood ratio. If this ratio is satisfied, we decide the class of instance $d$ as $c_1$. The point in this formulation is that we classify each instance only based on the loss values that we defined for each class. We do not use any assumption that is made by Naive Bayes to decide the class of an instance in this approach.
\subsection{Exercise}
\begin{enumerate}
\item Cook up a 2-class 4 binary feature dataset where the features are not conditionally independent of each
other given the class and yet the Naive Bayes classifier makes the right classification. Explain why.
\item Cook up a 2-class 3 binary feature dataset where the optimal Bayes classifier and the Naive Bayes
classifier are the same. Explain why.
\item Given a 2-class problem and a binary feature dataset, prove that the Naive Bayes assumption
essentially translates to a linear classifier, i.e., the classifier predicts class $c_1$ if a linear function of the features is
$> 0$ and the other class $c_2$ if the same linear function is $\leq 0$. What is the implication of this observation? (Hint:
What datasets are difficult to classify using a linear classifier?)
\item Cook up a few 2-class binary datasets where the conditional independence assumption is strongly
violated, i.e., not only are the features not independent of each other (within a class), but they are actually
functions of one another. Work out the performance of the Naive Bayes classifier on these datasets and make
observations.
\item Muse about the performance of the Naive Bayes classifier on a rare class situation. In particular design a 2-class dataset where there is exactly one instance of one class and all the remaining instances are from a different class. What would the Naive Bayes classifier do in this case? Give examples of where it might make the right decisions and where it might make the wrong decisions.
\item The Iris dataset (https://archive.ics.uci.edu/ml/datasets/Iris) is one of the toy datasets in machine
learning (until some time back, it was considered a serious experimental benchmark). The features of this
dataset are continuous-valued. Design a Naive Bayes classifier under the assumption of Gaussian feature values
and report on the results. Separate the data into training and test (describe what percentages you used) and report
quantitative results after $k$-fold cross-validation. (Choose $k$ suitably.) Do you think the Gaussian assumption is
valid? Do you think the conditional independence assumption holds for this dataset?
\end{enumerate}
