\chapter{Linear Methods for Classification}
As described in the previous section, we are given a dataset $D$ of instances each belonging to a class and having some features. The classification task is to create a model to classify each instance into one of the $|c|$ classes. As we discussed in the previous section, the Naive Bayes classifier is considered as a generative classifier since it aims to find the the probability of features, given the class label, i.e. $P(\vec{f}|c)$. Using a generative classifier, we can make a hypothetical document only by using probabilities that are computed throughout the model generation. On the other hand, there is another type of classifiers that is known as discriminative classifiers. A discriminative classifier aims to calculate the probability of a class given all the features, i.e. $P(c|\vec{f})$. In this section, we focus on explaining discriminative classifier and we talk about linear methods for classification. The assumptions that are made for these classifiers are that inputs are either binary or continuous-valued and classes can be separated by a hyperplane. A hyperplane separates the space into half spaces. For instance, in Fig~\ref{fig:hyperplane} the blue line is the hyperplane that separates the dot instances from the cross instances.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/hyperplane.jpg}
\caption{The blue line is the hyperplane that separates the dot instances from the cross instances}
\label{hyperplane}
\end{figure}
However, not all the real world problems can be separated simply by a hyperplane. As an example, consider the following distribution of samples in the $x1$ and $x2$ space. These samples cannot be separated using a hyperplane. However, if we transform the space from Cartesian to polar, we will be able to separate the two classes using a hyperplane.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/transformation.jpg}
\caption{There is no single hyperplane that we can use to separate the samples in the Cartesian space but if we transform the instances into the polar space, we can separate them using a hyperplane.}
\label{fig:transformation}
\end{figure}

\section{Logistic Regression}
The first linear method that we explain in this section is the Logistic Regression classifier. This classifier is considered as a discriminative classifier, therefore it find the probability of a class given features, i.e. $P(c|\vec{f})$. Let's assume a two-class classification problem with classes $c_1$ and $c_2$. The logistic regression finds the $P(c|\vec{f})$ as follows:
\begin{eqnarray*}
P(c_1|f_1,f_2,\ldots,f_{|F|}) = {{\exp(w_0 + \sum\limits_{i=1}^{|F|} w_i f_i)}\over{1 + \exp(w_0 + \sum\limits_{i=1}^{|F|} w_i f_i)}}
\label{eq1}
\end{eqnarray*}
As you can see in the above equation, the logistic regression takes into account a linear combination of features, i.e. $w_0+\sum_{i=1}^{|F|}w_i f_i$. Using the above equation, the posterior probability of the second class is then given by
\begin{eqnarray*}
P(c_2|f_1,f_2,\ldots,f_{|F|}) = {1\over{1 + \exp(w_0 + \sum\limits_{i=1}^{|F|} w_i f_i)}}
\label{eq2}
\end{eqnarray*}
In the above equation, the function $f(x) = \frac{1}{1+exp(x)}$ is called the logistic or sigmoid function and it is bounded between [0,1].\\
There are some assumptions that are made in logistic regression. The first one is that we consider the linear combination of features to determine the class of an instance. Moreover, according to the sigmoid function, we are assuming that the probability has a particular shape which is determined by the sigmoid function. The second assumption that is made is that all the features must be numeric values. Even if a specific feature has some discrete values, we should come up with a way to make it multipliable and addable.\\
Similar to the Naive Bayes approach, there is a decision rule for classifying an instance into class $c_1$ or $c_2$ as follows:
\begin{eqnarray*}
P(c_1|f_1,f_2,\ldots,f_{|F|}) & > & P(c_2|f_1,f_2,\ldots,f_{|F|})
\end{eqnarray*}
Then, substitute the equations for $P(c_1|\vec{f})$ and $P(c_2|\vec{f})$ into the above decision rule:
\begin{eqnarray*}
{{\exp(w_0 + \sum\limits_{i=1}^{|F|} w_i f_i)}\over{1 + \exp(w_0 + \sum\limits_{i=1}^{|F|} w_i f_i)}} & > & {1\over{1 + \exp(w_0 + \sum\limits_{i=1}^{|F|} w_i f_i)}} \\
\exp(w_0 + \sum\limits_{i=1}^{|F|} w_i f_i) & > & 1 \\
w_0 + \sum\limits_{i=1}^{|F|} w_i f_i & > & 0 \\
\end{eqnarray*}
Therefore, according to the above equation, we classify an instance into class $c_1$ if $w_0+\sum_{i=1}^{|F|}w_i f_i>0$. This is a linear classifier and the decision rule is based on a linear combination of features.\\
As you can see in the final equation for the decision rule, there are some weights attached to each feature. These are the important parameters that are required to give us the best hyperplane. But, the question is how are we going to find these parameters? Before answering this question let's look at the number of possible hyperplanes that can be used to separate two classes of data. As depicted in Fig.~\ref{fig:possiblehyperplanes}, the number of hyperplanes that we can choose to separate two classes of data is infinity. Therefore, we should ask whether there exist a hyperplane out of these infinite number of hyperplanes that works better than others in generalization of the model? The answer is yes! The logical option to choose the best hyperplane is to choose a hyperplane that is in the middle of the instances of the two classes. If we follow this idea for choosing a hyperplane, it will lead to a classifier known as Support Vector Machine (SVM).\\
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/possiblehyperplanes.jpg}
\caption{There is no single hyperplane that we can use to separate the samples in the Cartesian space but if we transform the instances into the polar space, we can separate them using a hyperplane.}
\label{fig:possiblehyperplanes}
\end{figure}

To find these weight we need to maximize the conditional likelihood or conditional log-likelihood of the $P(c|\vec{f},{\bf w})$:
\begin{displaymath}
\arg\max_{\bf w} \prod_{j=1}^{|D|} P(\textrm{class}(d_j) | f_1(d_j), f_2(d_j), ..., f_{|F|}(d_j), {\bf w})
\end{displaymath}
\begin{displaymath}
\arg\max_{\bf w} \sum_{j=1}^{|D|} \log P(\textrm{class}(d_j) | f_1(d_j), f_2(d_j), ..., f_{|F|}(d_j), {\bf w})
\end{displaymath}
This equation is going over all the instances and it tries to find the likelihood of a data or its class given its features and the weight ${\bf w}$. The assumption here is that all the instances are independent and therefore we can multiply all of the posterior probabilities together. To solve the above maximization problem, we will simplify some of the notation that is used in the equation. Let's define a response variable $r_j$ as follows:
\begin{itemize}
\item $r_j=1$ if $class(d_j)=c_1$
\item $r_j=0$ if $class(d_j)=c_2$
\end{itemize}
We are trying to maximize the log likelihood, i.e. $\arg\max_{\bf w} {\mathcal L}({\bf w})$. We re-write the maximization problem as follows:
\begin{eqnarray*}
\arg\max_w \sum_{j=1}^{|D|} r_j \log P(r_j=1 | {\bf f_{*}(d_j),w} ) + (1-r_j) \log P(r_j=0 |  {\bf f_{*}(d_j),w} )
\end{eqnarray*}
In the above equation, when $r_j$ is one, we consider $P(r_j=1 | {\bf f_{*}(d_j),w} )$ and if $r_j$ is zero, we consider $P(r_j=0 |  {\bf f_{*}(d_j),w} )$. By reducing the above equation:
\begin{eqnarray*}
\arg\max_w \sum_{j=1}^{|D|} r_j \log \frac{P(r_j=1 | {\bf f_{*}(d_j),w} )}{P(r_j=0 | {\bf f_{*}(j),w} )} + \log P(r_j=0 |  {\bf f_{*}(d_j),w} )
\end{eqnarray*}
{\bf Note}: If we have three classes, then we will not be able to use this notation to classify instances. We will explain how to deal with more than two classes scenarios later in this chapter.\\
If we substitute the Equation.~\ref{eq1} and Equation.~\ref{eq2}, into this equation:
\begin{eqnarray*}
\arg\max_w \sum_{j=1}^{|D|} r_j \log \exp(w_0 + \sum_{i=1}^{|F|} w_i f_i(d_j)) + \log {\frac{1}{1 + \exp(w_0 + \sum_{i=1}^{|F|} w_i f_i(d_j))}}\\
\arg\max_w \sum_{j=1}^{|D|} r_j(w_0 + \sum_{i=1}^{|F|} w_i f_i(d_j)) - \log (1 + \exp(w_0 + \sum_{i=1}^{|F|} w_i f_i(d_j)))
\label{eq3}
\end{eqnarray*}
The above maximization problem is a non-linear function in $w_i$. To maximize this function, we can use the gradient ascent 
approach to find the point that this function is maximized. In the gradient ascent approach, it simply starts with a random point and in each step it takes small step towards the point that maximizes the function (Fig.~\ref{fig:gradient}).
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/gradient.jpg}
\caption{The gradient ascent and gradient decent approach to find the maximum or minimum of a function.}
\label{fig:gradient}
\end{figure}
Before the maximization process, we can represent the Equation.~\ref{eq3} using the matrix notation. To do this, we will add a vector of ones as the $f_0$ feature into the dataset, i.e. $f_0={\bf 1}$:
\begin{eqnarray*}
\arg\max_w \sum_{j=1}^{|D|} r_j {\bf w^T f_{*}(d_j)} - \log (1 + e^{({\bf w^T f_{*}(d_j)})})
\end{eqnarray*}
Therefore, having the following log-likelihood function:
\begin{eqnarray*}
{\mathcal L}({\bf w}) & = & \sum_{j=1}^{|D|} r_j {\bf w^T f_{*}(d_j)} - \log (1 + e^{({\bf w^T f_{*}(d_j)})})
\end{eqnarray*}
If we take the derivative of the log-likelihood function for each weight $w_k$:
\begin{eqnarray*}
\frac{\partial {\mathcal L}({\bf w})}{\partial w_k} & = & \sum_{j=1}^{|D|} r_j f_k(d_j) - f_k(d_j) P(r_j = 1 |{\bf f_{*}(d_j),w} )\\
 & = & \sum_{j=1}^{|D|} f_k(d_j) (r_j - P(r_j = 1 |{\bf f_{*}(d_j),w})) \\
\end{eqnarray*}
By looking at the above equation, we can see that $r_j$ is compared with the probability of a being in class one given the features and weights, i.e. $P(r_j = 1 |{\bf f_{*}(d_j),w})$. Therefore, we are trying to minimize the difference between $r_j$ and the probability of being in class one, $P(r_j = 1 |{\bf f_{*}(d_j),w})$. On the other hand, we multiply this difference by the actual value of the feature $k$. This determines the amount of contribution that feature $k$ would have on bringing this difference down. Therefore, using the idea of gradient ascent the actual update would be look like:
\begin{eqnarray*}
w_i \leftarrow w_i + \eta \sum_{j=1}^{|D|} f_k(d_j) (r_j - P(r_j = 1 |{\bf f_{*}(d_j),w})) \\
\end{eqnarray*}
In the above equation, $\eta$ identifies the small steps that we would take to reach the maximization point. The $\eta$ value should be a small value and by following the above update rule we will find the hyperplane that separate the two classes. The good thing about this equation is that assuming the "fine-print", it is guaranteed that it will converge. The fine-print means that the $\eta$ value should be small.\\
As you can you see in this approach, it is not looking for a hyperplane that is residing in the middle of two classes. But, it tries to maximize the log likelihood under the logistic assumption of the shape of the probability.\\
\subsection{More than two class scenario}
As mentioned earlier, for the case of more than two class scenario we can not use the above-mentioned approach. Since there is no single hyperplane that can separate three classes at the same time. As each hyperplane separates the space into two half spaces, we can find $|C|-1$ hyperplanes to separate each each class from $c_{|C|}$:
\begin{eqnarray*}
\log \frac{ P(c_1|f_1,f_2,\ldots,f_{|F|})}{P(c_{|C|}|f_1,f_2,\ldots,f_{|F|})} & =  & w_0 + \sum\limits_{i=1}^{|F|} w_i f_i\\
\log \frac{ P(c_2|f_1,f_2,\ldots,f_{|F|})}{P(c_{|C|}|f_1,f_2,\ldots,f_{|F|})} & = & \ldots\\
\log \frac{ P(c_3|f_1,f_2,\ldots,f_{|F|})}{P(c_{|C|}|f_1,f_2,\ldots,f_{|F|})} & = & \ldots
\end{eqnarray*}
After finding all the above probabilities, we can aggregate the decisions of each model to identify the class of an instance. Fig.~\ref{fig:threeclasses} represents the hyperplane that is created using this approach.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/threeclasses.jpg}
\caption{The hyperplane for separating a three-class problem.}
\label{fig:threeclasses}
\end{figure}

\subsection{Cheating linear assumption}
When the boundary of each class is not a linear boundary, using a linear classifier will not help in classifying the instances. However, we can still use a linear classifier to separate each instance. Until now, we were dealing with features like $f_1$, $f_2$, and $f_3$. However, we can introduce new features using any combination of these features. For instance, we can add a feature $f_4$ which is the multiplication of $f_1$ and $f_2$, i.e. $f_4 = f_1 \times f_2$. This way we are adding non-linear features to our problem. Now, if we use the linear classifier we would be able to find non-linear boundaries amongst classes. Fig.~\ref{fig:cheatinglda} represents this  idea. As you can see in this figure, there is not much of a difference between the two approach. However, one should note that adding more non-linear features and higher order of a feature will cause a generalization problem known as overfitting. In the overfitting problem, the model is trained so well that separates almost every instances of the two classes, perfectly. But, as soon as we introduce new samples to test the model, it will fail to classify them, correctly. In the overfitting problem, the model tries to learn both the pattern that is governed on each class and all the noise samples that exist in data. In Fig.~\ref{fig:overfitting} we can simply fit a straight line with some degrees of error to interpolate the samples or we can find a complex polynomial that passes through each and every sample in the data. As you can see in this figure, the red line is prone to a lot of errors because of the possible noises in the data.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/cheatinglda.jpg}
\caption{The left figure is the boundary from cheating LDA by adding quadratic features into the dataset. The right figure is the boundary that is found by QDA}
\label{fig:cheatinglda}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/overfitting.jpg}
\caption{The straight line gives a better generalization rather than the polynomial line.}
\label{fig:overfitting}
\end{figure}
Therefore, although a complex boundary might have a good separation in the train data, it will have a bad result on the test dataset. This means that sometimes a simpler model is better than a complex model. This concept is known as Occam's razor. Based on the Occam's razor, if there are two hypothesis that fit the data the same, one should pick the one that is simpler.\\
Based on this idea, there is a rule that one should follow to make a balance between maximizing an objective measure and minimizing the penalty that it will give to a complex model. This idea is called regularization.

\section{Linear Discriminant Analysis (LDA)}
In the previous section, we explained the logistic regression classifier as one the linear and discriminative classifier. In this section, we will talk about another discriminative classifier known as Linear Discriminant Analysis (LDA). As mentioned in the previous section, a discriminative classifier finds the probability of $P(c_1|\vec{f})$:
\begin{eqnarray*}
P(c_1|f_1,f_2,\ldots,f_{|F|}) & = & {P(f_1,f_2,\ldots,f_{|F|}|c_1) P(c_1)\over{\sum_{c=1}^{|C|}P(f_1,f_2,\ldots,f_{|F|}|c) P(c)}}\\
\end{eqnarray*}
LDA assumes that all the features are distributed with a Gaussian distribution. Therefore, the probability of $P(f_1,f_2,\ldots,f_{|F|}|c_1)$ can be written as:
\begin{eqnarray*}
P(f_1,f_2,\ldots,f_{|F|}|c_1) & = & {1\over{(2 \pi)^{|F|/2} |{\bf \Sigma_1}|^{1/2}}} e^{-{{1}\over{2}} ({\bf f-\mu_1})^T {\bf \Sigma_1}^{-1} ({\bf f-\mu_1})} 
\label{eq4}
\end{eqnarray*}
where in the above equation, $\mu_1$ is the vector of mean for each feature for class $c_1$, ${\bf \Sigma_1}$ is the covariance matrix for class $c_1$, and $|{\bf \Sigma_1}|$ is the determinant of the covariance matrix. Another assumption that is made by LDA is that the covariance matrices of all the classes are the same:
\begin{eqnarray*}
{\bf \Sigma_1 = \Sigma_2 = \cdots = \Sigma}
\end{eqnarray*}
Just like the previous section, we use the logarithm of the posterior probabilities to make a decision about the class label:
\begin{eqnarray*}
\log {{P(c_1|f_1,f_2,\ldots,f_{|F|})}\over{P(c_2|f_1,f_2,\ldots,f_{|F|})}} & = & \log {{P(f_1,f_2,\ldots,f_{|F|}|c_1) P(c_1)}\over{P(f_1,f_2,\ldots,f_{|F|}|c_2) P(c_2)}}
\end{eqnarray*}
If we substitute the Equation.~\ref{eq4} into the above equation:
\begin{eqnarray*}
\log {{P(c_1|f_1,f_2,\ldots,f_{|F|})}\over{P(c_2|f_1,f_2,\ldots,f_{|F|})}} & = & \log {{P(f_1,f_2,\ldots,f_{|F|}|c_1) P(c_1)}\over{P(f_1,f_2,\ldots,f_{|F|}|c_2) P(c_2)}} \\
& = & \log {P(c_1) \over P(c_2)} {e^{-{{1}\over{2}} ({\bf f-\mu_1})^T {\bf \Sigma}^{-1} ({\bf f-\mu_1})} \over {e^{-{{1}\over{2}} ({\bf f-\mu_2})^T {\bf \Sigma}^{-1} ({\bf f-\mu_2})}}}\\
& = & \log {P(c_1) \over P(c_2)} - \underbrace{{1\over 2} ({\bf \mu_1^T \Sigma^{-1} \mu_1 - \mu_2^T \Sigma^{-1} \mu_2})}_{constant} +\\
& & {\bf f^T} \underbrace{{\bf \Sigma^{-1}(\mu_1 - \mu_2)}}_{constant}
\end{eqnarray*}
As you can see in the above equation, the resulting equation is linear in terms of the feature vector. We can also re-write the equation as follows:
\begin{eqnarray*}
\log {{P(c_1|f_1,f_2,\ldots,f_{|F|})}\over{P(c_2|f_1,f_2,\ldots,f_{|F|})}} & = & \log {P(c_1) \over P(c_2)} - {1\over 2} ({\bf \mu_1 + \mu_2)^T \Sigma^{-1} (\mu_1 - \mu_2)^T} +\\
& & {\bf f^T \Sigma^{-1}(\mu_1 - \mu_2)}
\label{eq5}
\end{eqnarray*}
Fig.~\ref{fig:lda} represents how LDA works on a dataset that each class has the same covariance matrix but the mean of each class is different. The LDA algorithm, learn the hyperplanes that are represented by the dark line and the classifier looks like the figure in the right.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/lda.jpg}
\caption{The left figures represent the distribution of each class. You can see that each class has the same covariance but the mean of each class is different. The right figure shows the actual data that is sampled from these three distribution and the hyperplanes that is separating these three classes.}
\label{fig:lda}
\end{figure}
In the LDA method, one of the strong assumption that is made is that all the classes have equal covariance. If we assume that the equal covariance does not hold for a dataset, we can use another method which is called Quadratic Discriminant Analysis (QDA). The reason for the quadratic part of this method is that we have two different covariance matrices, i.e. $\Sigma_1$ and $\Sigma_2$. Therefore, if a dataset has a quadratic boundary between two classes, the QDA would be able to find that boundary. An example of a dataset with two different covariance matrices is shown in Fig.~\ref{fig:differentcovariance}.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/differentcovariance.jpg}
\caption{Example of two classes of data with different covariance matrices}
\label{fig:differentcovariance}
\end{figure}
As another example, Fig.~\ref{fig:samemean} represents a case where two classes have the same mean but different variance. This is another example that LDA fails to classify data since all the distinction between two classes is coming from the variance not the mean. Although for a point on the right side of the x-axis, LDA can determines the class of the instance with a high probability, for point near the mean of data it fails to find the correct class.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/samemean.jpg}
\caption{Example of two classes of data with different covariance matrices and same mean. The point on the right can be easily classified by LDA, however the point on the mean could belong to each of the two classes.}
\label{fig:samemean}
\end{figure}
By looking at the Equation.~\ref{eq5}, we can see that we are taking the inverse of the covariance matrix. But, does the inverse of this matrix always exist? In order for a matrix to have an inverse it should satisfy some conditions. On the other hand, the covariance matrix is known to be positive semi-definite\cite{} [PROVE REQUIRED]. Therefore, we can easily take the inverse of this matrix.\\
{\bf Positive Semi-Definite (PSD) matrix}: Matrix ${\bf A}$ is PSD, if for every vector ${\bf x}\neq {\bf 0}$, ${\bf x}^T{\bf Ax}\geq 0$.\\
According to the Equation.~\ref{eq5}, the parameters that are required to be calculated are the $\mu_1$, $\mu_2$, and $\Sigma$. We can calculate each of these parameters using the following estimations:
\begin{eqnarray*}
\hat{\mu_1} & = & \sum_{i, \textrm{class}(i) = c_1} f(d_i)\\
\hat{\mu_2} & = & \sum_{i, \textrm{class}(i) = c_2} f(d_i)\\
\hat{\Sigma} & = &  \sum_{j=1}^{|C|} \sum_{i, \textrm{class}(i)=j} {1\over{|D| - |C|}} (f(d_i) - \hat{\mu_j}) (f(d_i) - \hat{\mu_j})^T
\end{eqnarray*}
The equation for calculating the $\hat{\Sigma}$ is often called \emph{Pooled Covariance} [WHY SHOULD WE FIND POOLED COVARIANCE NOT THE COVARIANCE?].

\subsection{Quadratic Discriminant Analysis (QDA)}
One the strict assumption in LDA is the equal covariance assumption. This assumption is not true for most of the real-world datasets. In the case that we consider two different covariance matrices representing each class, another method emerges which is called Quadratic Discriminant Analysis (QDA). Similar to the LDA, the QDA decision rule is as follows:
\begin{eqnarray*}
\log {{P(c_1|f_1,f_2,\ldots,f_{|F|})}\over{P(c_2|f_1,f_2,\ldots,f_{|F|})}} & = & \log {P(c_1) \over P(c_2)} - {1\over 2} \log |{\bf \Sigma_1}| + {1\over 2} \log |{\bf \Sigma_2}|\\
& & - {1\over 2} {\bf (f - \mu_1)^T \Sigma_1^{-1} (f - \mu_1)}\\
& & + {1\over 2} {\bf (f - \mu_2)^T \Sigma_2^{-1} (f - \mu_2)}
\end{eqnarray*}
In QDA, the classifier learns a quadratic boundary rather than a linear one. The estimation of these parameters are the same as the one presented in LDA. QDA is considered as the general model for LDA, however when it estimates two covariance matrices, it would be prone to the estimation errors and overfitting problem. There is another problem attached to both LDA and QDA. These two methods have a high bias and low variance. The formal definition of these these two concepts will be discussed in the future chapters. The reason for high bias is that they have a strong predefined notion of what the boundary of each class should be. Bias is something that you assume even before seeing the data. As a result of this strong bias, these two methods have a low variance which means if you have some noise and outliers in the data, it would not affect the performance of them much. Therefore, the price of having high bias in a method is that it will bring low variance as a result. In most of the data mining application, we should make a trade-off between these two parameters. As an example of a low biased and high variance algorithm, we can mention $k$ nearest neighbor (KNN) algorithm.
\subsection{Combining LDA and QDA}
We can interpolate between LDA and QDA. In this approach, we introduce a variable $0 \leq \alpha \leq 1$. We use the following equation to move from pure LDA to pure QDA by changing the $\alpha$ variable.
\begin{eqnarray*}
\hat{\Sigma}_k (\alpha) & = & \alpha \hat{\Sigma}_k + (1-\alpha) \hat{\Sigma}
\label{eq6}
\end{eqnarray*}
When $\alpha=1$ we will have LDA and when $\alpha=0$ we will have QDA algorithm. Fig.~\ref{fig:interpolate} shows the performance of the this approach as we increase the $\alpha$ value and we move from LDA to QDA. As you can see in this figure, as we move from LDA to QDA the error is decreasing, however after the point $\alpha=0.9$, the misclassification error will increase again. This means that the best interpolation between the two methods occur at $\alpha=0.9$. The reason for increasing the misclassification error on the QDA side is the overfitting problem.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/interpolate.jpg}
\caption{Interpolating between QDA and LDA. You can see that the misclassification error is increased after point $\alpha=0.9$ due to the overfitting problem.}
\label{fig:interpolate}
\end{figure}
We can add a more extreme assumption to the LDA method and test the performance of this approach on different datasets. In this approach, we assume that all the off-diagonal entries of the covariance matrix are zero and all the diagonal entries are the same:
\begin{eqnarray*}
\Sigma = \sigma^2 I
\end{eqnarray*}
If we combine this idea with the interpolation of QDA and LDA, the Equation.~\ref{eq6} changes to the following equation:
\begin{eqnarray*}
\hat{\Sigma}_k (\alpha,\beta) & = & \alpha \hat{\Sigma}_k + (1-\alpha) [ \beta \hat{\Sigma} + (1-\beta) \hat{\sigma}^2 I ] 
\end{eqnarray*}
Overall, the LDA approach works on the assumption that the mean of each class is different and it will fails on datasets were the mean of two classes are the same or close to each other but the variance is different. Therefore, we need other approaches to deal with this problem.

\section{Fisher's optimal linear discriminant}
As mentioned in the previous section, LDA will fail to classify classes that have similar or close mean value. The idea behind Fisher's linear discriminant is to find a plane that when you project all the points on that plane, we can separate the two classes using one single value. This plane should be selected in a way that when we project all the points in that plane, the same class points be together as close as possible and classes be separated as far as possible. Therefore, we are seeking to minimize the within class scatter and maximize the between-class scatter. For instance, the plane that is shown in Fig.~\ref{} is not a good projection plane for this dataset. Since you can see that there are overlapping points from both classes on this plane. As an example of a good plane for this dataset, we can consider the x-axis as the projection plane that separates the classes. The important thing about this approach is that it projects all data points into a single scalar value on a plane. This means that we transfer all the data from the $|F|$-dimension space to the one-dimension space and we should know that due to this transformation, we will lose a lot of information.\\
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section2/projection.jpg}
\caption{The projection of all the points in the dataset on a vector. The vector that is shown in this figure is not a good projection vector since after projection most of the data will overlap between the two classes}
\label{fig:projection}
\end{figure}
The projection of a point ${\bf p}$ onto a vector ${\bf v}$ is given by the following equation:
\begin{eqnarray*}
{\bf ({{v^T p}\over{v^T v}}) v}
\end{eqnarray*}
Fig.~\ref{projectionproof} provides the geometric concept behind this equation. In this figure, we want to project point ${\bf p}$ onto vector ${\bf v}$. Let's denote the perpendicular distance from point ${\bf p}$ to vector ${\bf v}$ as ${\bf y}$. The projection of this point onto ${\bf v}$ is represented by a vector as ${\bf x}$. We can represent the vector ${\bf x}$ as a scalar $\alpha$ times to the vector ${\bf v}$, i.e. ${\bf x}=\alpha \times {\bf v}$. On the other hand, ${\bf y}$ can be written as ${\bf p}-\alpha {\bf x}$. Now, we use the fact that ${\bf x}$ and ${\bf y}$ are perpendicular to each other:
\begin{eqnarray*}
{\bf x}^T.{\bf y}=0\\
(\alpha{\bf v})^{T}.({\bf p}-\alpha{\bf v})=0\\
\alpha {\bf v}^T {\bf p} - \alpha^2{\bf v}^T{\bf v}=0\\
\alpha ={\bf {{v^T p}\over{v^T v}}}\\
{\bf x} = {\bf ({{v^T p}\over{v^T v}}) v}
\end{eqnarray*}
The basic idea behind this method is to find a vector to maximize the separability of projections of both classes. Let's ${\bf w}$ denotes the projection vector that maps features from "f" space to "g" space:
\begin{eqnarray*}
{\bf g = w^T f}
\end{eqnarray*}
In the above equation, ${\bf w}$ and ${\bf f}$ are vectors and ${\bf g}$ is a scalar value which represents the projection. Moreover, we can project the mean of each feature onto the vector using the following equation:
\begin{eqnarray*}
{\bf \widetilde{\mu_1} = w^T \mu_1}
\end{eqnarray*}
where ${\bf \widetilde{\mu_1}}$ is calculated as follows:
\begin{eqnarray*}
{\bf \widetilde{\mu_1}} = \frac{1}{N_1}\sum_{class 1} g = \frac{1}{N_1}\sum_{class 1} {\bf w}^{T}f ={\bf w}^T\frac{1}{N_1}\sum_{class 1} f =  {\bf w}^T\mu_1
\end{eqnarray*}
where $N_1$ is the number of instances in class 1. According to the above equation, we can find the mean for class 2 the same way that we calculated for class 1.\\
In order to find the variance, we will use the unnormalized variance or scatter:
\begin{eqnarray*}
{\bf \widetilde{s}^{2}_{1} = \sum_{\textrm{class}_1} (g - \widetilde{\mu_1}) (g - \widetilde{\mu_1})^T}=\\
\sum_{class 1}({\bf w}^{T}f-{\bf w}^{T}\mu_1)({\bf w}^{T}f-{\bf w}^{T}\mu_1)^T=\\
\sum_{class 1}{\bf w}^{T}(f-\mu)(f-\mu)^T{\bf w}=\\
{\bf w}^{T}\underbrace{\sum_{class 1}(f-\mu)(f-\mu)^T}_{matrix}{\bf w}=\\
\end{eqnarray*}
The $\sum_{class 1}(f-\mu)(f-\mu)^T$ is often called $S_1$ and it is a square matrix. Since ${\bf w}$ is a vector, the whole equation leads to a scalar value. We can calculate the scatter for class two in the same way.\\
According to the Fisher's idea, we can use the following criterion to find the best projection plane:
\begin{eqnarray*}
{\bf J(w) = {{(\widetilde{\mu_1} - \widetilde{\mu_2})^2}\over{\widetilde{s_1}^2 + \widetilde{s_2}^2}}}
\end{eqnarray*}
The above equation is called the \emph{Fisher's criterion}. As you can see in this equation, in the nominator we are trying to maximize the distance between the means of two classes and minimize the variances of them. Therefore, the nominator represents the between-class scatter and the denominator shows the within class scatter. According to this criterion, there is one vector ${\bf w}$ that maximize this equation. In general, the vector that maximizes the above equation could be find using the following equation:
\begin{eqnarray*}
{\bf w^* } & = & {\bf \arg \max \left( {{w^T S_B w}\over{w^T S_w w}} \right) }
\end{eqnarray*}
Let's substitute the equations for $\widetilde{\mu_1}$ and $\widetilde{\mu_2}$ in the above equation:
\begin{eqnarray*}
{\bf J(w) = {{(w^T\mu_1- w^T\mu_2)^2}\over{\widetilde{s_1}^2 + \widetilde{s_2}^2}}}=\\
{\bf J(w) = {{w^T\overbrace{(\mu_1-\mu_2)(\mu_1-\mu_2)^T}^{S_B}w}\over{\widetilde{s_1}^2 + \widetilde{s_2}^2}}}=\\
\end{eqnarray*}
Now, we substitute the equation for scatter in the denominator:
\begin{eqnarray*}
{\bf\widetilde{s}_{1}^{2}+\widetilde{s}_{2}^{2}=  w^T S_1 w + w^T S_2 w= w^T\underbrace{(S_1+S_2)}_{S_w}w}
\end{eqnarray*}
Now, let's find the vector that maximizes $\left( {{w^T S_B w}\over{w^T S_w w}} \right)$:
\begin{eqnarray*}
{\bf \frac{\partial J}{\partial w} = \frac{\partial}{\partial w}.\frac{w^T S_B w}{w^T S_w w}} = 0\\
{\bf w^T S_w w \frac{\partial}{\partial w}(w^T S_B w)-w^T S_B w \frac{\partial}{\partial w}(w^T S_w w)} = 0\\
{\bf w^T S_w w (2S_B w) - w^T S_B w (2S_w w)} = 0
\end{eqnarray*}
Let's divide both sides of the equation by ${\bf w^T S_w w}$:
\begin{eqnarray*}
{\bf  S_B w - (\frac{w^T S_B w}{w^T S_w w}) S_w w} = 0\\
{\bf  S_B w - J(w) S_w w} = 0
\end{eqnarray*}
Now, Let's divide both sides by $S_w$:
\begin{eqnarray*}
{\bf  S_w^{-1}S_B w - J(w) w} = 0
\end{eqnarray*}
The above equation is the generalized eigenvalue problem. In the eigenvalue problem, given a matrix ${\bf A}$, we are trying to find all the eigenvalues $\lambda$ and eigenvectors $x$ that satisfies the following equation:
\begin{eqnarray*}
{\bf  Ax=\lambda x}
\end{eqnarray*}
For the two classes scenario, the answer for ${\bf w^*}$ is as follows:
\begin{eqnarray*}
{\bf w^* } = {\bf S_w^{-1} (\mu_1 - \mu_2)}
\end{eqnarray*}
According to this equation, we are trying to find the vector that is the result of $\mu_1 - \mu_2$ and scale it using the matrix $S_w^{-1}$.
One of the similar classifiers that project the data in the same way that Fisher's method does is the SVM classifier. Nevertheless, in SVM classifier the projection is not on a single vector, but it is on an infinite number of vectors. As a result of this projection, even complex classes will project in a way that could be easily separable.
[TALK MORE ABOUT THE GENERALIZED EIGENVALUE PROBLEM AND SITUATIONS WHERE THE $S_w$ MATRIX IS NOT-INVERTIBLE]
\subsection{Generalization of LDA and Fisher's method to more than two classes}
