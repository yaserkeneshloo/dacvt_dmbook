\chapter{Regression}
In this section, we talk about the regression problem. Although this topic might be considered as an old topic, still it plays an important role in data mining. To explain the regression, let's start with an example. For instance, we would like to predict the credit score of a person, based on her personal information. Another example would be predicting the grade of a student using the information of the student in Massive Open Online Course (MOOCS). These information could be the number of assignment the student submitted, the time that she submitted the first assignment, and etc. Therefore, in all the other applications, instead of predicting a class label for an instance, we would like to find a continuous valued variable for each instance.\\
The process of learning in regression is pretty much the same as the classification. Given a training dataset, we learn a model and use the model on a test dataset and measure the performance of the model. On of the important differences of regression w.r.t classification is that in regression all the features should be turn to a nominal value. For instance, if we have a categorical feature, we should find a way to represent it as a nominal value [TALK ABOUT HOW TO DO THIS].\\
Let's represent the dataset as tuples $(x_1,y_1),\cdots,(x_N,y_N)$. This dataset contains one feature $x_i$ which is also called as predictor along with a value $y_i$ which is called response value\footnote{There are different names for the predictor such as regressor, covariate, feature, and independent variable. The response value is also called outcome and dependent variable}. We denote the regression function by the following:
\begin{eqnarray*}
\eta(x)=E(Y|x)
\end{eqnarray*}
This equation is the conditional expectation of $Y$ given x. The linear regression model assumes a specific linear form for the $\eta$ function as follows:
\begin{eqnarray*}
\eta(x)=\alpha + \beta x
\end{eqnarray*}
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section5/linear.jpg}
\caption{A dataset that does not have a perfect linear shape but we can still approximate it using a linear fit}
\label{fig:linear}
\end{figure}
where $\alpha$ and $\beta$ are constant values. In the linear regression, we are not assuming that the distribution of the data is linear, but we would like to find a linear equation to approximate the data. For instance, although the data in the Fig.~\ref{fig:linear} is not distributed linearly, we can still find a linear approximation for this data. Now, the question is how to find this line that best approximate the data? For instance, in Fig.~\ref{fig:twoline} what makes the line $L_1$ to provide a better approximation than $L_2$? The answer to this question lies in the displacement of each point from the predicted value of that point in the line. According to the line, for each instance we can predict a value and then we can find the difference of the predicted value and the actual value. The best line that can fit the data is the one that minimize this error. This approach is called the least square criteria and it has the following structure:
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section5/twoline.jpg}
\caption{The horizontal line has a bigger least square error w.r.t the linear line that follows the trend of data}
\label{fig:twoline}
\end{figure}
\begin{eqnarray*}
\hat{\beta}_0,\hat{\beta} = argmin_{\hat{\beta}_0,\hat{\beta}}\sum_{i=1}^{N}(y_i-\beta_0-\beta x_i)^2
\end{eqnarray*}
Using the above criteria, the goal is to find the estimations of $\beta_0$ and $\beta$ that will minimize the above equation. In order to do this, we can take the derivative of the least square equation w.r.t $\beta_0$ and $\beta$. This will result into the following estimations for $\beta_0$ and $\beta$ which are represented by $\hat{\beta}_0$ and $\hat{\beta}$:
\begin{eqnarray*}
&\hat{\beta} = \frac{\sum_{j=1}^N(x_i-\bar{x})y_i}{\sum_{j=1}^N(x_i-\bar{x})^2}&\\
&\hat{\beta}_0 = \bar{y} - \hat{\beta}\bar{x}&
\end{eqnarray*}
where $N$ is the number of instances in the dataset and $\bar{x}$ and $\bar{y}$ are the predictor mean and response mean. Using the above estimation for $\beta_0$ and $\beta$, the fitted line or the predicted values can be calculated using:
\begin{eqnarray*}
\hat{y_i} = \hat{\beta}_0 + \hat{\beta}x_i
\end{eqnarray*}
and the error of displacement between the predicted value and actual value is called the residuals which can be found using:
\begin{eqnarray*}
r_i = y_i - \hat{\beta}_0 - \hat{\beta}x_i
\end{eqnarray*}
The least square method has some similarities to the maximum likelihood model [PROVIDE THE EQUATIONS FOR ML ESTIMATOR] under some assumptions. The assumptions that are made in the least square method are as follows:
\begin{itemize}
\item The data is independent and identically distributed (i.i.d).
\item The residuals are distributed normally with mean zero and variance of $\sigma$, i.e. $r = N(0,\sigma)$
\end{itemize}
As mentioned before, in the least square method we take the vertical displacement of a point from the regression line. However, one could use the perpendicular distance from a point and the regression line. If we try to minimize this distance, it will result in another method [I'M NOT SURE ABOUT WHAT THIS METHOD WOULD BE].\\
The linear model either assumes that the regression function $\eta$ is linear or that the linear model is a reasonable assumption. If we have multiple features, we can use the matrix notation to represent the least square method. In this case, the $\eta$ function would look like the following:
\begin{eqnarray*}
f(x) = \beta_0 + \sum_{j=1}^{p} x_j \beta_j
\end{eqnarray*}
where $\beta_j$ is the predictor for feature $x_i$ and $\beta_0$ is the intercept. Using this function, if we apply the least square criterion, we will get the following result:
\begin{eqnarray*}
RSS(\beta)=\sum_{i=1}^{N}(y_i-f(x_i))^2 = \sum_{i=1}^{N}(y_i-\beta_0-\sum_{j=1}^{P} x_{ij}\beta_{j})^2 = {\bf (y-X\beta)}^T({\bf y-X\beta})
\end{eqnarray*}
where $RSS$ is the Residual Sum Square and $RSS(\beta)$ is called the lack-of-fit. Please note that in order to represent the $\beta$ in a matrix notation we need to add a {\bf 1} column vector to the beginning of the data matrix $X$. This column will provide the coefficients for $\beta_0$ and all the actual data provide coefficients for $\beta$. Again, we would like to find the $\beta$ that minimize the above $RSS(\beta)$:
\begin{eqnarray*}
& \frac{\partial RSS}{\partial \beta} = -2{\bf X}^T({\bf y-X\beta}) = {\bf X}^T({\bf y-X\beta})=0 &\\
&\hat{\beta}=({\bf X}^T{\bf X})^{-1}{\bf X}^T{\bf y}&
\end{eqnarray*}
The equation $\hat{\beta}=({\bf X}^T{\bf X})^{-1}{\bf X}^T{\bf y}$ is called the Normality equation. Using this estimation for $\hat{\beta}$, the fitted values of the training data would be as follows:
\begin{eqnarray*}
\hat{{\bf y}} = {\bf X\beta} = {\bf X}({\bf X}^T{\bf X})^{-1}{\bf X}^T{\bf y}
\end{eqnarray*}
As you can see in the above equation, the estimation of the response value, i.e. $\hat{{\bf y}}$, depends on the actual value of $y$ times to the matrix ${\bf X}({\bf X}^T{\bf X})^{-1}{\bf X}^T$. This matrix is called the Hat matrix and is represented as $H={\bf X}({\bf X}^T{\bf X})^{-1}{\bf X}^T$. This matrix has some properties that we can see in the following:
\begin{itemize}
\item ${\bf H} = {\bf H}^T$
\item ${\bf H} = {\bf H}\times {\bf H}$
\item ${\bf H}({\bf I}-{\bf H} = ({\bf I}-{\bf H}){\bf H} = 0$
\end{itemize}
The Hat matrix actually project the actual value $y$ to a space that is created by the input data $X$. This is represented in Fig.~\ref{fig:projection}.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section5/projection.jpg}
\label{fig:projection}
\end{figure}
\section{Gauss-Markov Theorem}
The least square criterion has some unique features that makes it a very strong method for regression. The least square is called BLUE which means it is Best Linear Unbiased Estimator. It is a linear estimator but based on this statement it is also an unbiased estimator. In an unbiased estimator, the expected value of the estimation should be equal to the actual value of it, i.e. $E(\hat{\beta})=\beta$. For instance, the sample mean is an unbiased estimator of the true mean, however the sampled variance is not an unbiased estimator of the true variance.\\
We know that $\beta$ is the coefficients of the linear regression model and $\hat{\beta}=({\bf X}^T{\bf X})^{-1}{\bf X}^T{\bf y}$ is the least square estimation for it. Based on the Gauss-Markov theorem, let's assume that $\theta=a^{'}\beta$ is a linear combination of $\beta$, for example $x_0^{'}\beta$. Assume that $\hat{\theta}=a^{'}\hat{\beta}$ is the least square estimation of $\theta$. This estimator is an unbiased estimator:
\begin{eqnarray*}
E(a^T\hat{\beta}) = E(a^T(X^TX)^{-1}X^Ty)&\\
=a^T(X^TX)^{-1}X^TX\beta&\\
=a^T\beta
\end{eqnarray*}
Based on the Gauss-Markov theorem, if there is any other linear estimator $\tilde{\theta}=c^{'}y$ that is unbiased for $\theta=a^{'}\beta$, then
\begin{eqnarray*}
Var(a^T\hat{\beta})\leq Var(c^Ty)
\end{eqnarray*}
{\bf Proof}: We begin the proof by finding the expected value of $\tilde{\theta}$:
\begin{eqnarray*}
E({\bf c^{'}y})={\bf c^{'}X}\beta={\bf a^{'}}\beta
\end{eqnarray*}
For any value of $\beta$:
\begin{eqnarray*}
({\bf c^{'}X-a^{'}})\beta = 0 \rightarrow {\bf c^{'}X=a^{'}}
\end{eqnarray*}
Now, let's find the difference of variance between $\tilde{\theta}$ and $\hat{\theta}$:
\begin{eqnarray*}
Var(\tilde{\theta}) - Var(\hat{\theta})=\\
\sigma^2\{{\bf c^{'}c-a^{'}(X^{'}X)^{-1}a}\}=\\
\sigma^2\{{\bf c^{'}c-c^{'}X(X^{'}X)^{-1}X^{'}c}\}=\\
\sigma^2{\bf c^{'}\{I-X(X^{'}X)^{-1}X\}c}=\\
\sigma^2{\bf c^{'}\{I-H\}c}\geq 0\\
\end{eqnarray*}
The matrix ${\bf I-H}$ is a positive semi definite (PSD) matrix. Therefore, for any $a\in \mathbb{R}^N$:
\begin{eqnarray*}
{\bf a^{'}(I-H)a = a^{'}(I-H)^{'}(I-H)a = b^{'}b \geq 0} & \blacksquare
\end{eqnarray*}
On the other hand, we claim that the least square estimator is BLUE which means that it has the best estimation among all the other estimator for $\beta$. To prove this claim, we assume that $\hat{\beta}$ is unbiased and $\tilde{\beta}$ is another unbiased linear estimator $\tilde{\beta} = {\bf Cy}$. We already proved that
\begin{eqnarray*}
Var(\hat{\beta}) \leq Var(\tilde{\beta})
\end{eqnarray*}
Let's find the expected value of $\tilde{\beta}$:
\begin{eqnarray*}
E(\tilde{\beta}) = {\bf CX}\beta = \beta \ \ for\ any\ \beta
\end{eqnarray*}
According to the above equation, ${\bf CX = I}$. Now, let's find $Var(\tilde{\beta}) - Var(\hat{\beta})$ based on the above equation:
\begin{eqnarray*}
Var(\tilde{\beta}) - Var(\hat{\beta}) = \\
\sigma^2\{{\bf CC^{'}-(X^{'}X)^{-1}}\}= \\
\sigma^2\{{\bf CC^{'}-CX(X^{'}X)^{-1}X^{'}C^{'}}\}= \\
\sigma^2\{C{\bf I-X(X^{'}X)^{-1}X^{'}}\}C^{'}= \\
\sigma^2{\bf CHC^{'}}\\
\end{eqnarray*}
[MORE EXPLANATION ON THE PROOF]\\
\section{Subset Selection}
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section5/allfeature.jpg}
\label{fig:allfeature}
\end{figure}
Although the least square estimation has a lot of good properties, it does not perform properly on dataset with a lot of features. In these type of datasets, the least square estimator easily faces the overfitting problem. On the other hand, not all the features are useful to make the prediction and it would be difficult to interpret the result with too many regressors. Therefore, we need to find a way to choose the best features that could be used in the prediction task. Let's assume a dataset with 8 features. There are $2^8-1=255$ subset of features that we can use to create the regression model. However, it is known that the model that has all the features has the best fit. Although this model might not have the best generalization on the test data, it creates the best fit. As shown in Fig.~\ref{fig:allfeatures}, we can see that the model that is generated using all the features has the least residual sum of square. However, the problem on finding the best model in this way is that we need to explore all the possible combination of features. For a dataset with $p$ features, we need to build $2^p-1$ models to find out the best model. This is not feasible for $p>40$. Therefore, we need to find another method in these cases.\\
There are three greedy approaches that we can use to find the fit that gives us the best generalization. In the Forward stepwise regression, we start from a null model and iteratively add the most significant predictor from the candidate predictors. Inversely, in the Backward stepwise regression, we start with the full model and remove the most insignificant predictor from current model. The last one is a mix of these two approaches where we add a feature and drop a feature based on a specific criterion. We will use different criterion to quantify the effect of a predictor on the regression model. Two of these criterion are the Akaike Information Criterion (AIC) and Bayesian Information Criterion (BIC). The equation for these two criterion are as follows:
\begin{eqnarray*}
AIC = -2\log(likelihood)+2k\Leftrightarrow n\log(SSE) + 2k \\
BIC = -2\log(likelihood)+\log(n)k\Leftrightarrow n\log(SSE)+\log(n)k
\end{eqnarray*}
where $SSE$ is the sum of square error and $k$ is the number of features that we end up using in the model.

\section{Shrinkage method}
In this section, we talk about methods that do the feature selection using the shrinkage method. In this approach, there are some terms that qualify the linear equation and there are other terms that try to shrinkage the coefficients as much as possible. Therefore, it tries to make some of the coefficients zero. There are different methods that exist in this area. We will talk about ridge regression in this section.

\subsection{Ridge Regression}
Ridge regression shrinks the regression coefficients by imposing a penalty on their size. It follows the same equation for minimizing the sum square error, however it also add a penalty term for each coefficient:
\begin{eqnarray*}
\hat{\beta}^{ridge} = argmin_{\beta}\{\underbrace{\sum_{i=1}^{N}(y_i-\beta_0-\sum_{j=1}^{p}x_{ij}\beta_j)^2}_{LS}+\lambda \sum_{j=1}^{p}\beta_j^2\}
\end{eqnarray*}
As you can see in the above equation, the first part of this minimization is the same as the least square method and the second part is the penalty that is applied in ridge regression. In this approach, we are trying to bring each of the coefficients to zero. Another way to represent the above minimization is as follows:
\begin{eqnarray*}
\hat{\beta}^{ridge} = argmin_{\beta} \sum_{i=1}^{N}(y_i-\beta_0-\sum_{j=1}^{p}x_{ij}\beta_j)^2\\
subject\ to\ \sum_{j=1}^{p}\beta_j^2 \leq t
\end{eqnarray*}
In the above minimization, the condition is a hypersphere centered on zero and it constraints the magnitude of the coefficient to the threshold value $t$.\\
The estimation of $\hat{\beta}^{ridge}$ that we find in ridge regression is sensitive to the scales. Therefore, if we normalize each feature or center them to zero, it will have effects on the value of coefficients. On the other hand, after centering the data ${\bf X}$ and response vector ${\bf y}$, the residual sum of square would be as follows:
\begin{eqnarray*}
RSS(\lambda)=({\bf (y-X\beta)^{'}(y-X\beta)}+\lambda \beta^{'}\beta
\end{eqnarray*}
The important thing to note in this equation is that matrix ${\bf X}$ does not have the ${\bf 1}$ column. The reason is that we will not shrinkage the intercept value. Therefore, using the matrix notation we can write the estimation for $\hat{\beta}^{ridge}$ as the following:
\begin{eqnarray*}
\hat{\beta}^{ridge} = ({\bf X^{'}X+\lambda I})^{-1}{\bf X^{'}y}={\bf X^{'}(XX^{'}+\lambda I)^{-1}y}
\end{eqnarray*}
As you can see in the above equation, due to the penalty that is added in ridge regression, we added $\lambda {\bf I}$ to the above equation. On a special condition where all features of ${\bf X}$ are independent and ${\bf X}$ has orthogonal columns, $\hat{\beta}^{ridge}= \frac{\hat{\beta}}{(1+\lambda)}$. There is no practical way to find out the best value for $\lambda$. One way to find this value is to use the cross-validation and then try different values for $\lambda$ and find the best value that will give us the best performance.\\
\subsection{Singular Value Decomposition}
To find what happens when we use the shrinkage methods to find the linear fit it is good to represent the equations using the Singular Value Decomposition (SVD). We briefly explain what is SVD and how it can help us to interpret the behavior of shrinkage methods in this section.\\
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section5/svd.jpg}
\caption{$f_{1}^{'}$ and $f_{2}^{'}$ are the new axis that is found on SVD and they explain the most variance in the data}
\label{fig:svd}
\end{figure}
Among almost all the decomposition methods, SVD is one of the commons methods that is used in the scientific literature. Let's assume that we have a dataset ${\bf X}$ with two features $f_1$ and $f_2$. The data is distributed as represented in Fig.~\ref{fig:svd}. As you can see in this figure, the data looks like a stretched ellipsoid and has more variance along one dimension and less among the other dimension. SVD finds new dimensions $f_1^{'}$ and $f_2^{'}$ that capture the most variance in the data. For instance, in Fig.~\ref{fig:svd}, most of the variance in the data is along the $f_1^{'}$. After finding this dimension, SVD tries to find the other dimension that explains all the other variances in the data, i.e. $f_2^{'}$.\\
Note that $f_1^{'}$ and $f_2^{'}$ create as much a coordinate system as $f_1$ and $f_2$. Just the same as the original coordinate system, $f_1^{'}$ and $f_2^{'}$ are orthogonal and every point in the original space can be recast in the new space. On the other hand, in SVD, each new dimension is a linear combination of all the dimensions in the original space, i.e.
\begin{eqnarray*}
f_1^{'}=\alpha_1 f_1 + \alpha_2 f_2\\
f_2^{'}=\beta_1 f_1 + \beta_2 f_2
\end{eqnarray*}
SVD usually used as a preprocessing tools to transfer the data to a new dimension where the first dimension explains the most variance in the data, the second dimension explains all the other variances that is left and it keeps continue till we reach a point that all variance in the data is explained. SVD has usages in Principal Component Analysis (PCA) where we represent the matrix ${\bf XX^{'}=VD^2V^{'}}$. It also has usages in Latent Semantic Analysis (LSA) which is one of the usage of SVD in text mining. Although it might not seems natural to have an interpretation of linear combination of words, it provides good result both on the feature selection part and on performance of this approach in text mining.\\
The reason that we brought this introduction on SVD is that we can also explain the ridge regression using the SVD. The singular value decomposition of a matrix ${\bf X}$ has the form of ${\bf X=UDV^{'}}$ where ${\bf U}$ and ${\bf V}$ are orthogonal matrices of size $N\times p$ and $p \times p$. The columns of ${\bf U}$ span the column space of ${\bf X}$, and the columns of ${\bf V}$ span the row space. ${\bf D}=diag\{d_1,d_2,\cdots,d_p\}$ is a diagonal matrix with the condition that $d_1 \geq d_2 \geq \cdots \geq d_p \geq 0$. Just like the least square estimation that provide the best linear estimation among all the other approaches, SVD is the best decomposition that we can use among all the other decomposition methods. Although the SVD projects the data into another coordinate system, its performance is different with approaches like PCA. Therefore, we cannot apply PCA on the data and use the $k$ best features of it to do a regression and expect that result would be the same as the ridge regression result. Since each of the dimension in the new system is a linear combination of all the original dimensions and because it retains the information about all the other dimension, it is different from PCA.\\
Now, if we compare how the ridge regression is different from the least square method in terms of the SVD, we can see the following two equations:
\begin{eqnarray*}
\begin{array}{cccc}
{\bf X}\hat{\beta}^{ls}={\bf X(X^{'}X)^{-1}X^{'}y} & & & {\bf X}\hat{\beta}^{ridge}={\bf X(X^{'}X+\lambda I)^{-1}X^{'}y}\\
{\bf UU^{'}y} & & & = {\bf UD(D^2+\lambda I)^{-1}DU^{'}y}\\
=\sum_{j=1}^{p}u_j u_j^{'}y & & & =\sum_{j=1}^{p}u_j\frac{d_j^2}{d_j^2+\lambda} u_j^{'}y
\end{array}
\end{eqnarray*}
As we can see in the above equation, in the ridge regression, using the shrinkage of $\frac{d_j^2}{d_j^2+\lambda}$, it tries to shrink the coefficients. It does not try to shrink them down to zero, but it will shrink them to a small number. The smaller the $d_j$ is, the more the shrinkage would be. $d_j$ indicates the variation of the direction $u_j$. Thus $u_j$ with smaller variation will have smaller $\beta_j^{ridge}$ and therefore become less significant. As mentioned before, using this approach the response tend to vary most in the directions of high variance of the inputs.\\
We can also compare the ridge regression method and the least square method using the degrees of freedom that each approach provide for us. The least square estimation has the following degrees of freedom:
\begin{eqnarray*}
df_{ls} = tr({\bf H})=p
\end{eqnarray*}
where $H$ is the hat matrix and $p$ is the number of regressors. However, the degrees of freedom in ridge regression depends on $\lambda$ and as you increase this value, you emphasize more on the sparsity constraint, therefore the number of degrees of freedom will go down.
\begin{eqnarray*}
df_{ridge}(\lambda) = tr({\bf X(X^{'}X+\lambda I)^{-1}X^{'}})=tr({\bf H}_\lambda)=\sum_{j=1}^{p}\frac{d_j^2}{d_j^2+\lambda}
\end{eqnarray*}

\subsection{Least Absolute Shrinkage And Selection Operator: LASSO}
In the ridge regression equation, we can replace the quadratic terms for coefficient with the norm 1 of the coefficients. This will create another method which is called LASSO. This will enforce more sparsity in the estimated coefficients. The minimization problem in LASSO is as follows:
\begin{eqnarray*}
\hat{\beta}^{Lasso}=argmin_\beta\{\sum_{i=1}^{N}(y_i-\beta_0-\sum_{j=1}^{p}x_{ij}\beta_j)^2+\lambda \sum_{j=1}^p |\beta_j|\}
\end{eqnarray*}
Just like the ridge regression, we can rewrite the above minimization problem as the following form:
\begin{eqnarray*}
& \hat{\beta}^{Lasso}=argmin_\beta\{\sum_{i=1}^{N}(y_i-\beta_0-\sum_{j=1}^{p}x_{ij}\beta_j)^2 &\\ 
& subject\ to\ \sum_{j=1}^{p}|\beta_j|\leq t &
\end{eqnarray*}
The constraint on the LASSO regression is more like a square rather than the hypersphere in the ridge regression. Moreover, unlike ridge regression where we can find a close form derivation of the least square equation, in LASSO it would be a bit difficult to take the derivatives. As a solution to this problem, there are methods known as Homotopy methods that usually solve methods by making them harder! In this approach, it will take the minimization equation of the LASSO regression and rather solving it for one value of $\lambda$, it will find the solution for all values of $\lambda$. Therefore, $\lambda$ is not a single value anymore, it becomes a range of values. This approach starts from $\lambda=0$, which is equal to the least square fit, and then it varies $\lambda$ and track the change of the estimation error. Therefore, it will eventually solve this for infinite number of $\lambda$ and then we can decide which value of $\lambda$ gives us the minimum error. Fig.~\ref{fig:lasso}, represent a schematic of this approach.\\
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section5/lasso.jpg}
\caption{The plot of the $\lambda$ vs the estimated error in the Homotopy method.}
\label{fig:lasso}
\end{figure}
The other approach that we can use to find the best estimator is a measure called shrinkage factor [NEED TO BE DEFINED FROM THE BOOK]. As you can see in the equation for this factor, the shrinkage factor depends on $\lambda$. As shown in Fig.~\ref{fig:shrinkage}, as the shrinkage factor increases, we can see how the coefficients change for different features. As shown in Fig.~\ref{fig:shrinkage}, we can see that at certain point of the shrinkage factor only a subset of features are non-zero and many of these features have zero coefficient values. Based on this figure, we can see that LASSO perform better in terms of bringing the coefficients to zero w.r.t the ridge regression approach.\\
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section5/shrinkage.jpg}
\caption{The plot of the shrinkage factor vs the coefficient of the regressors.}
\label{fig:shrinkage}
\end{figure}
If we think about the three methods that we discussed till now for subset selection, i.e. best subset selection, ridge regression, and LASSO, we can think of them as approaches that threshold the $\beta$ in different ways. Table.~\ref{table:comparison} provides a comparison of these approaches in terms of the the way they apply thresholding on $\beta$.
\begin{table}[!bh]
\begin{center}
\begin{tabular}{llc}
\hline
Estimator & Formula\\
\hline\rule{0pt}{12pt}
Best Subset (size M) & $\hat{\beta}_j I (rank(|\hat{\beta}_j|\leq M))$ & hard-thresholding\\
Ridge & $\hat{\beta}_j/(1+\lambda)$ &\\
LASSO & $sign(\hat{\beta}_j)(|\hat{\beta}_j|-\lambda)$ & soft-thresholding\\
[2pt]
\hline
\end{tabular}
\end{center}
\label{table:comparison}
\end{table}
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section5/comparison.jpg}
\caption{The difference between best subset selection, ridge regression, and LASSO}
\label{fig:comparison}
\end{figure}
Moreover, Fig.~\ref{fig:comparison} represents the difference of these approaches. As you can see in this figure, LASSO provides a softer thresholding on the $\beta$ values. In Fig.~\ref{fig:constraint}, we can see the difference in the constraint  regions of the ridge regression and LASSO method. As mentioned before, in the ridge regression the constraint has a hypersphere shape and the LASSO method has a square shape on the constraint. We are finding the estimated parameter in the $\beta$ space. As shown in this figure, in the case of ridge regression, we are trying to find a contour that is intersecting with the constraint region, whereas in LASSO we will find a single point that intersect with the constraint region.\\
So far, we have seen two different shrinkage methods. We can generalized the idea that used in these two approach and write the minimization equation as follows:
\begin{eqnarray*}
\hat{\beta}=argmin_\beta\{\sum_{i=1}^{N}(y_i-\beta_0-\sum_{j=1}^{p}x_{ij}\beta_j)^2+\lambda \sum_{j=1}^p |\beta_j|^q\}
\end{eqnarray*}
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section5/constraint.jpg}
\label{fig:constraint}
\end{figure}
where for $q=1$ it generates the LASSO method and if $q=2$, it generates the ridge regression method. Fig.~\ref{fig:contour} represents different shapes that we can get by varying the value of $q$.\\
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section5/contour.jpg}
\label{fig:contour}
\end{figure}

\subsection{Least Angle Regression}
This is another method of regression that is based on the QR decomposition. The QR decomposition tries to find one best direction to explain the dataset but not in terms of variance. Then it continues to find another direction which is orthogonal to the first one and it keeps continue to do that. In this approach, we start by normalizing all the predictors in a way that they have mean zero and unit variance. Then starting with the residual $r =y-\bar{y}, \beta_1, \beta_2, \cdots, \beta_p = 0$, we find the predictor $x_j$ that is most correlated to $r$. We then move $\beta_j$ from zero towards its least square coefficient $\langle x_j,r \rangle$ until some other competitor $x_k$ has as much correlation with the current residual as does $x_j$. We continue this process, by moving $\beta_j$ and $\beta_k$ in the direction defined by their joint least squares coefficient of the current residual on $(x_j,x_k)$, until some other competitor $x_l$ has as much correlation with the current residual. The  whole process continues until all $p$ predictors have been entered. After $min(N-1,p)$ steps, we arrive at the full least-square solution.