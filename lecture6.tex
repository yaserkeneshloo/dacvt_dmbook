\chapter{Model Selection}
In this section, we explain different model selection methods. The question that we are trying to answer is that given a dataset and different models for fitting the data, which one is better and how to compare them? Without model selection, there will not be a quantitative way to compare different classifiers. Most of these models choose a model with a view towards improving generalization performance, penalizing complexity, and rewarding parsimony. The first framework that we talk about in this section is Bayes Decision Theory and then we talk about the Bias-Variance Decomposition. It is worth nothing to mention that there a lot of other frameworks like Akaike Information Criterion (AIC), Bayesian Information Criterion (BIC), Minimum Description Length (MDL), and VC Dimension that can be used for this task.
\section{Bayes Decision Theory}
The first framework that we talk about is the Bayes Decision Theory. In the Bayes model, we are interested in finding the probability of the model given a dataset, i.e. $P(model|data)$:
\begin{eqnarray*}
P(model|data)=\frac{P(data|model)\times P(model)}{P(data)}
\end{eqnarray*}
Using this equation, for two different models $m_1$ and $m_2$, we should compare $P(m_1|data)$ with $P(m_2|data)$. Since $P(data)$ is constant, the probability of the model depends on $P(data|model)\times P(model)$. As you can see in this equation, there are two important factors in determining $P(model|data)$. The probability of data given model, i.e. $P(data|model)$ and prior probability of the model $P(model)$. Based on this framework, in the face of data, even if you start with a strict assumption on the distribution of the prior, when you start seeing evidence in contrary, you should update the prior probability into a posterior probability. We will use a simple coin toss example to represent this idea. In this example we want to determine whether a coin is fair or not. We define the null and alternative hypothesis as follows:
\begin{eqnarray*}
\mbox{$H_0$: coin is fair}\\
\mbox{$H_1$: coin is loaded (H)}
\end{eqnarray*}
In this example, the alternative hypothesis is that the coin is loaded in a way that probability of heads is greater than tails. At the beginning, let think that we start with a strict assumption that the prior probability of getting heads is as Fig~\ref{fig6:prior} (Top Left):
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section6/prior.png}
\caption{The prior probability of the coin before seeing any data}
\label{fig6:prior}
\end{figure}
The x-axix is the bias and y-axix represents the probability. If bias is zero, the coin always falls in tail, if the bias is one the coin falls in head, and if the bias is $0.5$ it means that it is a fair coin. Since we do not know which of these three hypothesis are true, we assume that every possible hypothesis is true. According to the top left figure, we assume that every hypothesis is equally likely. Let's assume that we see a head in the first coin toss. This immediately rules out the hypothesis that the coin is biased toward falling always on tail. We now calculate the posterior with this single observation:
\begin{eqnarray*}
posterior \propto prior \times likelihood = 1 \times H = H
\end{eqnarray*}
You can see the new posterior probability in Fig~\ref{fig6:prior} (Top Center). Again, let's assume that this time we toss the coin and we face a tail. We can then immediately rule out the hypothesis that the coin is loaded towards falling heads. Let's calculate the posterior for this observation:
\begin{eqnarray*}
posterior \propto prior \times likelihood = H \times (1-H) = H
\end{eqnarray*}
The posterior after the second toss looks like the Middle Center figure in Fig~\ref{fig6:prior}. If we do this experiment $N$ times and receive $R$ heads and $N-R$ tails, the posterior probability would have the following structure:
\begin{eqnarray*}
posterior \propto H^R(1-H)^{N-R}
\end{eqnarray*}
To understand the shape of this posterior probability, we set the derivative to zero and second derivative to be negative:
\begin{eqnarray*}
\frac{d posterior}{d X}|_{X_0}=0\\
\frac{d^2 posterior}{d X^2}|_{X_0}<0
\end{eqnarray*}
Since finding this derivative is hard, we can take the log of posterior function and then find the derivative of it:
\begin{eqnarray*}
\log{posterior} \propto R\log{H} + (N-R)\log{1-H}\\
d \log{posterior} \propto \frac{R}{H}-\frac{N-R}{1-H}=0 \rightarrow H = \frac{R}{N} = \frac{\# heads}{\#tosses}
\end{eqnarray*}
To find the second derivative of this function we use the Taylor's series expansion:
\begin{eqnarray*}
&L=\log_c{prob(X|data)}&\\
&L=L(X_0)+\underbrace{\frac{dL}{dX}|_{X_0}(X-X_0)}_{zero}+\frac{1}{2}\frac{d^2L}{dX^2}|_{X_0}(X-X_0)^2+\cdots&
\end{eqnarray*}
In the above equation, $L(X_0)$ is a constant that shifts the function and therefore is negligible. Since we took the log of this function, if we use the exponential to represent the actual function, it will looks like a Gaussian function:
\begin{eqnarray*}
prob(X|data)\approx A \exp(\frac{1}{2}\frac{d^2L}{dX^2}|_{X_0}(X-X_0)^2)
\end{eqnarray*}
where $A = exp(L(X_0)$. The Gaussian function has the following form:
\begin{eqnarray*}
prob(x|\mu,\sigma)=\frac{1}{\sigma \sqrt{2\pi}}\exp(-\frac{(x-\mu)^2}{2\sigma^2})
\end{eqnarray*}
On the other, we can show that variance of this function is as follows [PROVE IT]:
\begin{eqnarray*}
Variance \propto \frac{H(1-H)}{N}
\end{eqnarray*}
According to this equation, if the coin is fair, i.e. $H=0.5$, the variance is maximum. It means that it will take more observation to convince that the coin is fair rather than biased.\\
According to the Bayes Decision Theory, whether you start with a completely wrong prior or a similar prior to the posterior, if you observe a large number of samples the posterior converges to the actual posterior probability of coin toss.\\
Now, let's suppose that we want to compare two different models $M_1$ and $M_2$. Using the Bayes Decision Theory, we can quantify the better model using the following equation:
\begin{eqnarray*}
\frac{p(M_1|data)}{p(M_2|data)} <> 1
\end{eqnarray*}
If we expand this equation, we will get the following equation:
\begin{eqnarray*}
\frac{p(M_1|data)}{p(M_2|data)}=\underbrace{\frac{p(data|M_1)}{p(data|M_2)}}_{\mbox{likelihood ratio}}\times \underbrace{\frac{p(M_1)}{p(M_2)}}_{\mbox{prior ratio}}
\end{eqnarray*}
In the past chapters of this book, we have seen lots of probabilistic models. Therefore, we can easily quantify the likelihood ratio. However, the question is how to get the prior for each model. Let's assume that $M_1$ has no parameter and $M_2$ has 1 parameter which help it to be more general. For instance, assume that $M_1$ is the model representing $y=2x$ and $M_2$ is the model representing $y=ax+b$. We can see that the second model with two extra parameters has more degrees of freedom than the first one. However, we should come up with a way to penalize models that have too many parameters and are complex. We can write the likelihood function of a model as the marginalize function of it over all possible parameters that it takes:
\begin{eqnarray*}
p(data|model)=\int_{\lambda} p(data,\lambda|model)d\lambda
\end{eqnarray*}
where $\lambda$ represents all the parameter for this model. We can re-write the above equation as follows:
\begin{eqnarray*}
p(data|model)=\int_{\lambda} p(data|model,\lambda)\times p(\lambda|model)d\lambda
\end{eqnarray*}
To formulate this equation, we should know about the prior probability of $\lambda$ and also we should find the probability of data given the model and $\lambda$. To find the prior, we can think of $\lambda$ as it takes some values between $\lambda_{min}$ and $\lambda_{max}$ and it has a prior like Fig~\ref{fig6:priorlambda}. Therefore, the prior probability is as follows:
\begin{eqnarray*}
p(\lambda|model)=\frac{1}{\lambda_{max}-\lambda_{min}}
\end{eqnarray*}
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section6/priorlambda.png}
\caption{The prior probability of the coin before seeing any data}
\label{fig6:priorlambda}
\end{figure}
The probability of data given model and $\lambda$ can be easily computed since we fix the parameter $\lambda$ and calculate this probability using the model. If we go back to the equation for comparing the two models, we can re-write the equation based on the above calculations:
\begin{eqnarray*}
\frac{p(M_1|data)}{p(M_2|data)}=\frac{p(data|M_1)}{p(data|M_2)}\times \frac{p(M_1)}{p(M_2)}=\frac{p(data|M_1)}{p(data|M_2)}\times \frac{\lambda_{max}-\lambda_{min}}{1}\times\frac{p(M_1)}{p(M_2)}
\end{eqnarray*}
As we can see in the above equation, $M_2$ is the model that has an extra parameter called $\lambda$ and we can see that $\lambda_{max}-\lambda_{min}$ is multiplied by the probability of the data given model $M_1$. This means that if model $M_2$ uses a broad range of $\lambda$, it will benefit model $M_1$. Therefore, the more flexibility the model $M_2$ have, it will create a broad range for $\lambda$ and it will cause model $M_2$ to always be the loser model. The term $\frac{\lambda_{max}-\lambda_{min}}{1}$ is called Occam's factor and it will control the complexity of a model. This equation is also true for model $M_1$ and if it has some parameter, it will help model $M_2$ to the winning model.

\section{Bias-Variance Decomposition}
In this section, we talk about another model selection method which is called Bias-Variance Decomposition. In this method, we try to decompose the expected value of loss into three components: Noise, Bias, and Variance. In the previouse section, we defined Bias as all the assumptions that we assume for the model before seeing any data. As mentioned in previous sections, a method with a high bias usually has low variance and vice versa. As an example of a high biased method is the regression classifier.\\
We want to define a loss function and quantify the loss function in terms of bias and variance. Let's start by viewing the model as an estimator of the target value $y$:
\begin{eqnarray*}
L(y,M({\bf x}))=I(M({\bf x})\neq y) = \left\{ 
  \begin{array}{l l}
  0 & \quad \text{if }M({\bf X})=y \\    
  1 & \quad \text{if }M({\bf x})\neq y
  \end{array} \right.
\end{eqnarray*}
where ${\bf x}$ is the data instance and $M$ is the model that we are inferring. Therefore, $M({\bf x})$ is the output of the model for input ${\bf x}$. We compare the output of the model with the actual label of the instance, i.e. $y$, using the loss function $I$. Based on this loss function, whenever the output of the model is not equal to the actual label, the loss value would be one. However, we should note that this loss function cannot be used for regression. In case of the regression, the output of the model is a continuous value. Therefore, we use the square difference of the model output and the actual label of the data to quantify the loss:
\begin{eqnarray*}
L(y,M(x))=(y-M(x))^2
\end{eqnarray*}
One could also think about using the absolute value of this difference, however taking derivative of a function with absolute value is harder than the quadratic form.\\
Using this loss function, we can now define the error. Error is defined as the expected loss. Therefore, given a particular input $x$, the model makes the output $M(x)$. We are required to compare the output with $y$, however since we are doing this comparison on our test data, we might not know the actual label of the input. If we knew the actual value of $y$, we could estimate the actual loss. Consequently, we find the expected value of $L(y,M(x))$ over all possible values of $y$:
\begin{eqnarray*}
E_y[L(y,M(x))|x]=\sum_y L(y,M(x)).P(y|x)
\end{eqnarray*}
where $P(y|x)$ is the probability that this value of $y$ is the actual value of $y$ given $x$. As an example of this loss function, look at the Fig~\ref{fig6:lasso}. This figure shows the prediction error vs. the model complexity. As you can see in this figure, as we increase the model complexity, the prediction error for the train data start decreasing. As you can see in this figure, the training error approaches to zero. It will have the exact zero value when the number of parameters in the model is the same as the number of data points and it does not have two rows with the same set of features belonging to different classes. However, at a certain point the prediction error will increase for test data, which is the result of overfitting the data. This is an example to how we can approach to empirically estimate the bias and variance. To quantify this estimation we follow the above equation:
\begin{eqnarray*}
E_y[L(y,M(x))|x]=E_y[I(y\neq M(x))|x]\\
=\sum_y I(y\neq c_i).P(y|x)\\
=\sum_{y\neq c_i}P(y|x)\\
=1-P(c_i|x)
\end{eqnarray*}
According to this equation, to minimize the expected loss, we should maximize the posterior class probability, $P(c_i|x)$.\\
As mentioned before, for regression we should use the square error between the model output and the actual value. Therefore, the expected loss for regression would be as follows:
\begin{flalign}
E_y[L(y,M(x,D))|x,D]&=E_y[(y-M(x,D))^2|x,D]&&\\\nonumber
&=E_y[(y-\underbrace{E_y[y|x]+E_y[y|x]}_{\text{add and subtract the same term}}-M(x,D))^2|x,D]&&\\\nonumber
&=E_y[(y-E_y[y|x])^2|x,D]+E_y[(M(x,D)-E_y[y|x])^2|x,D]&&\\\nonumber
&+E_y[2(y-E_y[y|x]).(E_y[y|x]-M(x,D))|x,D]&&\\\nonumber
&=E_y[(y-E_y[y|x])^2|x,D]+(M(x,D)-E_y[y|x])^2&&\\\nonumber
&+2(E_y[y|x]-M(x,D)).\underbrace{(E_y[y|x]-E_y[y|x])}_{0}&&\\\nonumber
&=\underbrace{E_y[(y-E_y[y|x])^2|x,D]}_{var(y|x)}+\underbrace{(M(x,D)-E_y[y|x])^2}_{squared-error}&&\nonumber
\label{eq6.1}
\end{flalign}
where $D$ is the dataset that is used to construct the model. Now, we can break down the equation for squared-error to the Bias and Variance:
\begin{flalign}
&E_D[(M(x,D)-E_y[y|x])^2]&&\\\nonumber
&=E_D[(M(x,D)-\underbrace{E_D[M(x,D)]+E_D[M(x,D)]}_{\text{add and subtract same term}}-E_y[y|x])^2]&&\\\nonumber
&=E_D[(M(x,D)-E_D[M(x,D)])^2]+E_D[(E_D[M(x,D)]-E_y[y|x])^2]&&\\\nonumber
&+2(E_D[M(x,D)]-E_y[y|x]).\underbrace{(E_D[M(x,D)]-E_D[M(x,D)])}_{0}&&\\\nonumber
&=\underbrace{E_D[(M(x,D)-E_D[M(x,D)])^2]}_{variance}+\underbrace{(E_D[M(x,D)]-E_y[y|x])^2}_{bias}&&\\\nonumber
\label{eq6.2}
\end{flalign}
This form of decomposition is the general form for a regression method. Using this equation, we can find the exact estimation for different type of regression[PROVIDE SOME EXAMPLE OF THE DECOMPOSITION FOR SPECIFIC REGRESSION METHOD]. Combining the Eq~\ref{eq6.1} and Eq~\ref{eq6.2} together, we will get the following equation for Bias-Variance Decomposition in regression method:
\begin{flalign}
&E_{x,D,y}[(y-M(x,D))^2]&&\\\nonumber
&=E_{x,D,y}[(y-E_y[y|x])^2|x,D]+E_{x,D}[(M(x,D)-E_y[y|x])^2]&&\\\nonumber
&=\underbrace{E_{x,y}[(y-E_y[y|x])^2]}_{noise}+\underbrace{E_{x,D}[(M(x,D)-E_D[M(x,D)])^2]}_{average\ variance}&&\\\nonumber
&+\underbrace{E_x[(E_D[M(x,D)]-E_y[y|x])^2]}_{average\ bias}&&\\\nonumber
\label{eq6.3}
\end{flalign}
Based on this equation, when we want to understand the performance of the regression on a given dataset, we should determine the above three quantities. Fig~\ref{fig6:biasvariance} represents how these three term will change accordingly when we change the model parameter $C$ for a given dataset. As you can see in this figure, as bias decreases the variance increases. Generally speaking, in the case of having high bias in a model, we have low variance and we are usually underfitting the data. Moreover, these models are more stable to changes. On the other hand, when the model has high variance, it usually has a low bias and we overfit the data. These models are also considered as unstable, because changing a point in these methods has a great impact on the result.\\
We should note that we can apply this decomposition to all methods and it is not restricted to classifiers and regression methods.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section6/biasvariance.png}
\caption{An example of how Bias and Variances change for regression classifier}
\label{fig6:biasvariance}
\end{figure}


\section{Ensemble Methods}
There are methods that try to reduce some of the problems of methods with high/low bias/variance. We explain some of these methods in this section. In these methods, we try to combine different classifiers with different bias and variance to get the result of the classification for a data.

\subsection{Bagging}
Bagging is considered as an ensemble method where it tries to wrap different classifiers and decide the class label using this bagged classifier. Bagging method usually helps with controlling the variance. In Bagging, we create multiple samples of data. Therefore, given a dataset $D$, Bagging creates a lot of samples of this dataset. Then, it creates a predictor for each of these sampled datasets:
\begin{eqnarray*}
v_j(x)=|\{M_i(x)=c_j|i=1,\cdots,K\}|
\end{eqnarray*}
where, $M_i(x)$ is the $i$-th model created for sample data $x$. After retrieving the result for all classifiers, it uses a voting mechanism to find the class label of a data:
\begin{eqnarray*}
M^K(x)=\argmax_{c_j}\{v_j(x)|j=1,\cdots,k\}
\end{eqnarray*}

\subsection{Boosting}
Another example of an ensemble method is the Boosting method. This method helps to reduce the Bias. For instance, when using a Naive Bayes, one might be interested in reducing the Bias of the model. In Boosting, we carefully select data points to emphasize hard-to-classify examples. In this method, we create a series of dataset. Starting from the original dataset $D$, we weight all the samples uniformly. In each iteration, we create a new dataset based on the samples that classified wrong in the previous iteration. In the new dataset, we change the weighting of sample in a way that wrong samples have higher weights than correct samples. Therefore, in each iteration we are trying to make wrong samples classified correctly.\\
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section6/adaboost.png}
\caption{The AdaBoost algorithm}
\label{fig6:adaboost}
\end{figure}

Boosting method can be cast as a method to combine weak classifiers to form a strong classifier. A weak classifier is a classifier that does just a little bit better than random chance. On the other hand, a strong classifier is a classifier that given sufficient data its performance can be arbitrarily boosted. The boosting method proved that the class of weak classifier learnability and strong classifiers are the same. In addition, it creates a method that can convert a weak classifier to a strong classifier.\\
In this section, we explain the most famous Boosting method which is called AdaBoost. The algorithm for AdaBoost is depicted in Fig~\ref{fig6:adaboost}. As mentioned earlier, AdaBoost starts with the original dataset and each samples have equal weights. In each iteration of the algorithm, it weights all the samples that classified incorrectly accordingly using the classifier error. This process continues till a point that the error for classifier does not change much. After the end of this process, we are end up with $K$ different classifiers that has an associated weight with them:
\begin{eqnarray*}
v_j(x)=\sum_{t=1}^{K}\alpha_t . I(M_t(x)=c_j)\\
M^K(x)=\argmax_{c_j}\{v_j(x)|j=1,\cdots,K\}\\
M^K(x)=sign(\sum_{t=1}^K \alpha_t M_t(x))
\end{eqnarray*}
where $\alpha_t$ is the weight to each classifier.\\
If we carefully look at the algorithm for Boosting, we can see that there is relation between Bagging and Boosting method. In Boosting, we change the weight of each sample in each iteration, however, in the Bagging the weights for all samples are the same in each iteration. Therefore, if the weight are the same for each iteration, Boosting will have the same performance as Bagging. An example of this classifier is shown in Fig~\ref{fig6:boosting}. As you can see in this example in each iteration, all the samples that are classified incorrectly have a larger weights that other samples (Top figure). In the bottom of this figure, we can see how the classifier decides the label of each point, using the three classifiers that are built during the algorithm.
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section6/boosting.png}
\caption{An example of the Boosting algorithm}
\label{fig6:boosting}
\end{figure}
As mentioned earlier, one of the advantage of the Boosting method is to reduce the Bias. However, interestingly this decrease in the Bias does not come with the cost of increasing Variance. In other words, Boosting decrease the Bias of the model, but does not increase the Variance as mush as other classifiers. [MAYBE WE CAN PUT MORE CONCRETE MATERIALS, HERE].

\subsection{Stacking}
In this section, we talk about yet another ensemble method which is called as Stacking. It does not have a scientific base as the Boosting and Bagging methods, but provides good result in some datasets. In Stacking, we learn different classifiers over our dataset. Then, it considers the decision of each classifier as a set of features. Afterward, it learn another classifier using all the features along with this new set of features. The point is that, when we use different classifiers some of them classify samples correctly and some others classify them wrongly. Therefore, we are hoping that these decisions could help us to build a new classifier that has a better performance than each of those single classifiers.\\

\section{VC-Dimension}
One of the problems of these methods is that when you ask about the complexity of a model, they translate it to the number of parameters in the model. However, the number of parameters is not always a good estimator for the complexity of a model. For instance, in Fig~\ref{fig6:sine} we can see that the function $sin(50x)$ separated the samples from two classes of data, i.e. green and blue class. Interestingly, irrespective of how the green samples and blue samples are aligned it this space, you can always find a $sin$ function with a suitable frequency that can separate the samples in this dataset. Obviously, this method has a high Variance and low Bias. This function only has one parameter and considered as a simple method. This examples shows that the number of parameter is not a good way to capture the complexity of a method. To fix this problem, Vapnik and Chervonenkis came up with a method which is called VC-dimension to formulate the complexity of a method.\\
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section6/sine.png}
\caption{How the sine function separates samples from two classes}
\label{fig6:sine}
\end{figure}

VC-dimension defined based on the shattering power of a hypothesis. A set $S$ of points is said to be shattered by a hypothesis space $H$, if $H$ can realize all possible dichotomies of $S$. As an example, suppose we have one features and binary class. Now, consider all the possible dataset that you can create using this feature and these two classes. Fig~\ref{fig6:dichotomies1} shows the all the possible dichotomies for two sample points and binary class. Suppose we would like to use a hyperplane to separate these points. We can simply separate the points using this hyperplane. However, if we add one more sample to this space and they are labeled as shown in Fig~\ref{fig6:dichotomies2}, we cannot use one hyperplane to separate these points.\\
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section6/dichotomies1.png}
\caption{Different dichotomies of two samples in for a binary class}
\label{fig6:dichotomies1}
\end{figure}

The goal of VC-dimension is to look at models not in terms of their parameter but in terms of how they shatter data points in a space. The VC-dimension is defined as the largest size data points $S$ that a model can shatter. To get the lower bound $C$ on VC-dimension, we just need to find one example dataset of size $C$ that can be shattered by the hypothesis. On the other hand, the upper bound $C$ on VC-dimension requires that no dataset of size $C+1$ can be shattered by the hypothesis. Here are some examples of the VC-dimension for different hypothesis:
\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/section6/hyperplane.png}
\caption{An example where a hyperplane cannot separate the with four samples in 2D dimension}
\label{fig6:hyperplane}
\end{figure}
\begin{itemize}
\item H=Intervals on the real line: The VC-dimension for this hypothesis is 2. As you can see in Fig~\ref{fig6:interval}, with two data points we can wrap an interval that separates the two classes of data. However, if we add another sample, one interval cannot be used to separate the data points.
\item H=hyperplanes in $R^d$: The VC-dimension for this hypothesis is $d+1$. Let's consider an example for 2D space or $R^2$. We can see that any formation of three points in this space can be shattered by a hyperplane. However, if we add one more sample like the data in Fig~\ref{fig6:hyperplane}, one hyperplane cannot shatter these four samples.
\item H=Axis-aligned rectangles in 2D space. The VC-dimension for this hypothesis is four.
\item H=A tree over 10 boolean variable. The VC-dimension for this hypothesis is $2^10$. The number of points that a decision tree can shatter is 1024. To understand why is this the case, think about all the possible dichotomies of 1024 samples with two classes. It is the possible to learn a tree for each of these dichotomies. Therefore, the VC-dimension of a decision tree with 10 features is 1024.
\item H=sine function: The VC-dimension for sine function is infinity. This is because no matter how we arrange a set of points on a single line, we can always find a sine function with a suitable frequency that can shatter the data points.
\end{itemize}
Vapnik also invented an approach called structural risk minimization where we try to use different models in order of increasing VC-dimension and then take the model that gives the minimum risk. For more information on VC-dimension, one should read papers on PAC (Probably Approximately Correct) learning.

\section{Other Methods}
There are other criteria for model selection. AIC and BIC are two of these model selection methods:
\begin{eqnarray*}
AIC = -\frac{2}{N}E[\log likelihood]+2\frac{d}{N}\\
BIC = -2\log likelihood + (\log N)d
\end{eqnarray*}
As you can see in the equation for AIC and BIC, the $\log likelihood$ determines the performance of the classifier and $2\frac{d}{N}$ and $(\log N)d$ control the complexity of the model for AIC and BIC, respectively.