\select@language {english}
\contentsline {chapter}{\numberline {1}Classification}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Applications of classification}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Input and Outputs}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Wrong assumption on classification}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Bayesian Approach}{3}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Naive Bayes}{4}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Text Classification using Naive Bayes}{6}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Bag of Words}{7}{subsection.1.4.3}
\contentsline {subsection}{\numberline {1.4.4}Laplace Smoothing}{8}{subsection.1.4.4}
\contentsline {subsection}{\numberline {1.4.5}Good-Turing estimator}{8}{subsection.1.4.5}
\contentsline {subsection}{\numberline {1.4.6}Bernoulli Model}{10}{subsection.1.4.6}
\contentsline {subsection}{\numberline {1.4.7}Feature Selection}{10}{subsection.1.4.7}
\contentsline {subsection}{\numberline {1.4.8}NB for continuous-valued features}{10}{subsection.1.4.8}
\contentsline {subsection}{\numberline {1.4.9}Evaluation Strategy}{11}{subsection.1.4.9}
\contentsline {subsection}{\numberline {1.4.10}Theoretical understanding of Naive Bayes}{13}{subsection.1.4.10}
\contentsline {subsection}{\numberline {1.4.11}Exercise}{15}{subsection.1.4.11}
\contentsline {chapter}{\numberline {2}Linear Methods for Classification}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Logistic Regression}{17}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}More than two class scenario}{23}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Cheating linear assumption}{23}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Linear Discriminant Analysis (LDA)}{25}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Quadratic Discriminant Analysis (QDA)}{28}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Combining LDA and QDA}{29}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Fisher's optimal linear discriminant}{29}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Generalization of LDA and Fisher's method to more than two classes}{33}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Tree Based Classifiers}{35}{chapter.3}
\contentsline {section}{\numberline {3.1}Tree Induction}{35}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Issues with tree learning}{37}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Continuous-valued attribute}{39}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Feature Selection in Decision Tree}{39}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Stopping Criteria}{40}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Bias and Variance}{41}{subsection.3.1.5}
\contentsline {chapter}{\numberline {4}$K$-Nearest Neighbor Classifier}{43}{chapter.4}
\contentsline {chapter}{\numberline {5}Regression}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}Gauss-Markov Theorem}{48}{section.5.1}
\contentsline {section}{\numberline {5.2}Subset Selection}{50}{section.5.2}
\contentsline {section}{\numberline {5.3}Shrinkage method}{51}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Ridge Regression}{51}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Singular Value Decomposition}{52}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Least Absolute Shrinkage And Selection Operator: LASSO}{54}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Least Angle Regression}{57}{subsection.5.3.4}
\contentsline {chapter}{\numberline {6}Model Selection}{59}{chapter.6}
\contentsline {section}{\numberline {6.1}Bayes Decision Theory}{59}{section.6.1}
\contentsline {section}{\numberline {6.2}Bias-Variance Decomposition}{63}{section.6.2}
\contentsline {section}{\numberline {6.3}Ensemble Methods}{66}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Bagging}{66}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Boosting}{67}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Stacking}{69}{subsection.6.3.3}
\contentsline {section}{\numberline {6.4}VC-Dimension}{69}{section.6.4}
\contentsline {section}{\numberline {6.5}Other Methods}{71}{section.6.5}
